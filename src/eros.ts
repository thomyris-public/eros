import { performance } from 'node:perf_hooks';

import { WriteInterface } from './write/write';
import { LoggerInterface } from './helpers/logger/logger';
import { SchemaInterface } from './getSchemaData/schema.interface';
import { ConfInterface } from './helpers/getJsonConfig/conf.interface';
import { GetSChemaDataInterface } from './getSchemaData/getSchemaData';
import { splitGqlSchemaInterface } from './splitGqlSchema/splitGqlSchema';
import { ErrorHandlerInterface } from './helpers/errorHandler/errorHandler';
import { GetJsonConfigInterface } from './helpers/getJsonConfig/getJsonConfig';

export interface ErosInterface {
  execute(): Promise<void>;
}

export class Eros implements ErosInterface {
  private getJsonConfig: GetJsonConfigInterface;
  private splitSchema: splitGqlSchemaInterface;
  private getSchema: GetSChemaDataInterface;
  private write: WriteInterface;
  private errorHandler: ErrorHandlerInterface;
  private logger: LoggerInterface;

  constructor(
    getJsonConfig: GetJsonConfigInterface,
    splitSchema: splitGqlSchemaInterface,
    getSchema: GetSChemaDataInterface,
    write: WriteInterface,
    errorHandler: ErrorHandlerInterface,
    logger: LoggerInterface,
  ) {
    this.getJsonConfig = getJsonConfig;
    this.splitSchema = splitSchema;
    this.getSchema = getSchema;
    this.write = write;
    this.errorHandler = errorHandler;
    this.logger = logger;
  }
  async execute(): Promise<void> {
    try {
      const start = performance.now();
      const conf: ConfInterface = await this.getJsonConfig.execute();
      const lines: string[] = await this.splitSchema.execute(conf);
      const schema: SchemaInterface = await this.getSchema.execute(lines);
      await this.write.execute(schema, conf);
      const duration = performance.now() - start;
      this.logger.time('Generated in', duration);
    } catch (error: any) {
      this.errorHandler.execute(error);
    }
  }
}
