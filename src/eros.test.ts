import { Eros } from './eros';
import { Write } from './write/write';
import { conf, schema } from './eros.test.data';
import { Logger } from './helpers/logger/logger';
import { WriteDoc } from './write/writeDoc/writeDoc';
import { FormatPath } from './helpers/formatPath/formatPath';
import { GetScalars } from './helpers/getScalars/getScalars';
import { WriteAssets } from './write/writeAssets/writeAssets';
import { GetSchemaData } from './getSchemaData/getSchemaData';
import { SplitGqlSchema } from './splitGqlSchema/splitGqlSchema';
import { ErrorHandler } from './helpers/errorHandler/errorHandler';
import { WriteDocIndex } from './write/writeDocIndex/writeDocIndex';
import { GetJsonConfig } from './helpers/getJsonConfig/getJsonConfig';
import { CreateTable } from './write/writeDoc/createTable/createTable';
import { GenFileHandler } from './helpers/genFileHandler/genFileHandler';
import { GetIndentation } from './helpers/getIndentation/getIndentation';
import { GetApiRequests } from './helpers/getApiRequests/getApiRequests';
import { WriteExample } from './write/writeDoc/writeExample/writeExample';
import { GenerateFromType } from './helpers/generateFromType/generateFromType';
import { CreateLinkToAssets } from './helpers/createLinkToAssets/createLinkToAssets';
import { WriteTableOfContent } from './write/writeDoc/writeTableOfContent/writeTableOfContent';
import { WriteRequestExample } from './write/writeDoc/writeExample/writeRequestExample/writeRequestExample';
import { WriteResponseExample } from './write/writeDoc/writeExample/writeResponseExample/writeResponseExample';

describe('Eros', () => {
  const genFileHandler = new GenFileHandler();
  const generateFromType = new GenerateFromType(genFileHandler);
  const getJsonConfig = new GetJsonConfig();
  const splitSchema = new SplitGqlSchema();
  const getSchema = new GetSchemaData();
  const getScalars = new GetScalars();
  const logger = new Logger();
  const errorHandler = new ErrorHandler(logger);
  const formatPath = new FormatPath();

  const writeAssets = new WriteAssets(generateFromType, formatPath);
  const getRequests = new GetApiRequests();
  const createlink = new CreateLinkToAssets(getJsonConfig, formatPath);
  const createTable = new CreateTable(createlink);
  const getIndentation = new GetIndentation();
  const writeRequest = new WriteRequestExample(getIndentation, generateFromType);
  const writeResponse = new WriteResponseExample(getIndentation, generateFromType);
  const writeExample = new WriteExample(writeRequest, writeResponse);
  const writeToc = new WriteTableOfContent();
  const writeDoc = new WriteDoc(getRequests, createTable, writeExample, writeToc, formatPath);
  const writeIndex = new WriteDocIndex(getRequests, getScalars, formatPath);
  const write = new Write(writeAssets, writeDoc, writeIndex);

  const eros = new Eros(getJsonConfig, splitSchema, getSchema, write, errorHandler, logger);

  const spyGetJsonConfig = jest.spyOn(getJsonConfig, 'execute');
  const spySplitSchema = jest.spyOn(splitSchema, 'execute');
  const spyGetSchema = jest.spyOn(getSchema, 'execute');
  const spyWrite = jest.spyOn(write, 'execute');

  const spyError = jest.spyOn(console, 'error');
  const spyInfo = jest.spyOn(console, 'info');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  /* eslint @typescript-eslint/no-empty-function: "off" */
  beforeAll(async () => {
    // avoid geting the log message in the terminal comment to see log in console
    spyError.mockImplementation(() => {});
    spyInfo.mockImplementation(() => {});
  });

  it('Should return test', async () => {
    // arrange
    spyGetJsonConfig.mockResolvedValueOnce(conf);
    spySplitSchema.mockResolvedValueOnce(['']);
    spyGetSchema.mockResolvedValueOnce(schema);
    spyWrite.mockResolvedValue();
    // act
    await eros.execute();
    // assert
    expect(spyGetJsonConfig).toHaveBeenCalledTimes(1);
    expect(spyGetJsonConfig).toHaveBeenCalledTimes(1);
    expect(spyWrite).toHaveBeenCalledTimes(1);
  });

  it('Should log an error because getJsonConfig.execute fail', async () => {
    // arrange
    spyGetJsonConfig.mockRejectedValue(new Error('error'));
    // act
    await eros.execute();
    // assert
    expect(getJsonConfig.execute).toHaveBeenCalledTimes(1);
    expect(spyError).toHaveBeenCalledTimes(1);
  });
});
