import { join } from 'path';
import { promises, existsSync } from 'fs';

import { WriteDocInterface } from './writeDoc/writeDoc';
import { WriteAssetsInterface } from './writeAssets/writeAssets';
import { SchemaInterface } from '../getSchemaData/schema.interface';
import { WriteDocIndexInterface } from './writeDocIndex/writeDocIndex';
import { ConfInterface } from '../helpers/getJsonConfig/conf.interface';

export interface WriteInterface {
  execute(schema: SchemaInterface, conf: ConfInterface): Promise<void>;
}

export class Write implements WriteInterface {
  private writeAssets: WriteAssetsInterface;
  private writeDoc: WriteDocInterface;
  private writeIndex: WriteDocIndexInterface;

  constructor(writeAssets: WriteAssetsInterface, writeDoc: WriteDocInterface, writeIndex: WriteDocIndexInterface) {
    this.writeAssets = writeAssets;
    this.writeDoc = writeDoc;
    this.writeIndex = writeIndex;
  }

  async execute(schema: SchemaInterface, conf: ConfInterface): Promise<void> {
    const path: string = join(conf.output.path, conf.output.dirName);

    // delete "doc" folder
    if (existsSync(path)) await promises.rm(path, { recursive: true });

    // create "doc" folder
    await promises.mkdir(path);

    // create "assets" folder and all it's content
    await this.writeAssets.execute(path, schema, conf);

    await this.writeDoc.execute(conf, path);
    await this.writeIndex.execute(conf, path);
  }
}
