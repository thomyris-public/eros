import * as fs from 'fs';

import { Write } from './write';
import { WriteDoc } from './writeDoc/writeDoc';
import { WriteAssets } from './writeAssets/writeAssets';
import { WriteDocIndex } from './writeDocIndex/writeDocIndex';
import { GetScalars } from '../helpers/getScalars/getScalars';
import { FormatPath } from '../helpers/formatPath/formatPath';
import { CreateTable } from './writeDoc/createTable/createTable';
import { WriteExample } from './writeDoc/writeExample/writeExample';
import { SchemaInterface } from '../getSchemaData/schema.interface';
import { GetJsonConfig } from '../helpers/getJsonConfig/getJsonConfig';
import { ConfInterface } from '../helpers/getJsonConfig/conf.interface';
import { GenFileHandler } from '../helpers/genFileHandler/genFileHandler';
import { GetApiRequests } from '../helpers/getApiRequests/getApiRequests';
import { GetIndentation } from '../helpers/getIndentation/getIndentation';
import { GenerateFromType } from '../helpers/generateFromType/generateFromType';
import { CreateLinkToAssets } from '../helpers/createLinkToAssets/createLinkToAssets';
import { WriteTableOfContent } from './writeDoc/writeTableOfContent/writeTableOfContent';
import { WriteRequestExample } from './writeDoc/writeExample/writeRequestExample/writeRequestExample';
import { WriteResponseExample } from './writeDoc/writeExample/writeResponseExample/writeResponseExample';

describe('Write', () => {
  const handleGenFile = new GenFileHandler();
  const generateFromType = new GenerateFromType(handleGenFile);
  const getConf = new GetJsonConfig();
  const formatPath = new FormatPath();
  const writeAssets = new WriteAssets(generateFromType, formatPath);
  const getRequests = new GetApiRequests();
  const getScalars = new GetScalars();
  const createlink = new CreateLinkToAssets(getConf, formatPath);
  const createTable = new CreateTable(createlink);
  const getIndentation = new GetIndentation();
  const writeRequest = new WriteRequestExample(getIndentation, generateFromType);
  const writeResponse = new WriteResponseExample(getIndentation, generateFromType);
  const writeExample = new WriteExample(writeRequest, writeResponse);
  const writeToc = new WriteTableOfContent();
  const writeDoc = new WriteDoc(getRequests, createTable, writeExample, writeToc, formatPath);
  const writeIndex = new WriteDocIndex(getRequests, getScalars, formatPath);
  const write = new Write(writeAssets, writeDoc, writeIndex);

  const spyRm = jest.spyOn(fs.promises, 'rm');
  const spyMkdir = jest.spyOn(fs.promises, 'mkdir');
  const spyExist = jest.spyOn(fs, 'existsSync');

  const spyWriteAssets = jest.spyOn(writeAssets, 'execute');
  const spyWriteDoc = jest.spyOn(writeDoc, 'execute');
  const spyWriteIndex = jest.spyOn(writeIndex, 'execute');

  const schema: SchemaInterface = {
    interface: {
      data: [],
    },
    type: {
      data: [],
    },
    input: {
      data: [],
    },
    enum: {
      data: [],
    },
    query: {
      data: [],
    },
    mutation: {
      data: [],
    },
    subscription: {
      data: [],
    },
    scalar: {
      data: [],
    },
  };

  const conf: ConfInterface = {
    app: {
      title: 'title',
    },
    maxDepth: 2,
    input: '',
    output: {
      path: './',
      dirName: 'test',
    },
  };

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should call mkdir and writeAssets', async () => {
    // arrange
    spyExist.mockReturnValue(false);
    spyMkdir.mockResolvedValue('');
    spyWriteAssets.mockResolvedValue();
    spyWriteDoc.mockResolvedValue();
    spyWriteIndex.mockResolvedValue();
    // act
    await write.execute(schema, conf);
    // assert
    expect(spyRm).not.toHaveBeenCalled();
    expect(spyMkdir).toHaveBeenCalledTimes(1);
    expect(spyWriteAssets).toHaveBeenCalledTimes(1);
    expect(spyWriteAssets).toHaveBeenCalledTimes(1);
    expect(spyWriteAssets).toHaveBeenCalledTimes(1);
  });

  it('Should call rmdir if folder already exist', async () => {
    // arrange
    spyExist.mockReturnValue(true);
    spyRm.mockResolvedValue();
    spyMkdir.mockResolvedValue('');
    spyWriteAssets.mockResolvedValue();
    spyWriteDoc.mockResolvedValue();
    spyWriteIndex.mockResolvedValue();
    // act
    await write.execute(schema, conf);
    // assert
    expect(spyRm).toHaveBeenCalledTimes(1);
    expect(spyMkdir).toHaveBeenCalledTimes(1);
    expect(spyWriteAssets).toHaveBeenCalledTimes(1);
    expect(spyWriteAssets).toHaveBeenCalledTimes(1);
    expect(spyWriteAssets).toHaveBeenCalledTimes(1);
  });
});
