import { join } from 'path';
import { promises } from 'fs';

import { WriteDoc } from './writeDoc';
import { CreateTable } from './createTable/createTable';
import { WriteExample } from './writeExample/writeExample';
import { FormatPath } from '../../helpers/formatPath/formatPath';
import { GetJsonConfig } from '../../helpers/getJsonConfig/getJsonConfig';
import { GetIndentation } from '../../helpers/getIndentation/getIndentation';
import { GetApiRequests } from '../../helpers/getApiRequests/getApiRequests';
import { GenFileHandler } from '../../helpers/genFileHandler/genFileHandler';
import { WriteTableOfContent } from './writeTableOfContent/writeTableOfContent';
import { GenerateFromType } from '../../helpers/generateFromType/generateFromType';
import { CreateLinkToAssets } from '../../helpers/createLinkToAssets/createLinkToAssets';
import { WriteRequestExample } from './writeExample/writeRequestExample/writeRequestExample';
import { WriteResponseExample } from './writeExample/writeResponseExample/writeResponseExample';
import { conf, emptyRequestData, filesData, filesDataWithError, requestsData, requestsDataError } from './writeDoc.test.data';

describe('WriteDoc', () => {
  const getConf = new GetJsonConfig();
  const getRequests = new GetApiRequests();
  const formatPath = new FormatPath();
  const createLink = new CreateLinkToAssets(getConf, formatPath);
  const createTable = new CreateTable(createLink);
  const getIndentation = new GetIndentation();
  const genFileHandler = new GenFileHandler();
  const generateFromType = new GenerateFromType(genFileHandler);
  const writeRequest = new WriteRequestExample(getIndentation, generateFromType);
  const writeResponse = new WriteResponseExample(getIndentation, generateFromType);
  const writeExample = new WriteExample(writeRequest, writeResponse);
  const writeToc = new WriteTableOfContent();
  const writeDoc = new WriteDoc(getRequests, createTable, writeExample, writeToc, formatPath);

  const spyGetRequests = jest.spyOn(getRequests, 'execute');
  const spyMkdir = jest.spyOn(promises, 'mkdir');
  const spyWriteFile = jest.spyOn(promises, 'writeFile');
  const spyWrite = jest.spyOn(writeDoc as any, 'write');
  const spyCreateTable = jest.spyOn(createTable, 'execute');
  const spyWriteGenFile = jest.spyOn(genFileHandler, 'write');

  const path = join(conf.output.path, conf.output.dirName);

  beforeEach(async () => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should call apiRequests and only write the folder pages', async () => {
    // arrange
    spyGetRequests.mockResolvedValueOnce(emptyRequestData);
    spyMkdir.mockResolvedValueOnce('');
    spyMkdir.mockResolvedValueOnce('');

    // act
    await writeDoc.execute(conf, path);
    //assert

    expect(spyGetRequests).toHaveBeenCalledTimes(1);
    expect(spyMkdir).toHaveBeenCalledTimes(1);
    expect(spyWrite).toHaveBeenCalledTimes(1);
    expect(spyWrite).toHaveBeenNthCalledWith(1, 'test/pages', [], conf);
    expect(spyWriteFile).toHaveBeenCalledTimes(0);
  });

  it('Should call getApiRequests and write 2 folders and 2 files', async () => {
    // arrange
    spyGetRequests.mockResolvedValueOnce(requestsData);
    spyMkdir.mockResolvedValueOnce('');
    spyMkdir.mockResolvedValueOnce('');
    spyMkdir.mockResolvedValueOnce('');

    spyWriteFile.mockResolvedValueOnce();
    spyWriteFile.mockResolvedValueOnce();
    // act
    await writeDoc.execute(conf, path);
    //assert

    expect(spyGetRequests).toHaveBeenCalledTimes(1);
    expect(spyMkdir).toHaveBeenCalledTimes(2);
    expect(spyWrite).toHaveBeenCalledTimes(2);
    expect(spyWrite).toHaveBeenNthCalledWith(1, 'test/test', [filesData], conf);
    expect(spyWrite).toHaveBeenNthCalledWith(2, 'test/pages', [filesData], conf);
    expect(spyWriteFile).toHaveBeenCalledTimes(2);
  });

  it('Should call getApiRequests and write 2 folders and 1 files containing errors string', async () => {
    // arrange
    spyGetRequests.mockResolvedValueOnce(requestsDataError);
    spyMkdir.mockResolvedValueOnce('');
    spyMkdir.mockResolvedValueOnce('');
    spyMkdir.mockResolvedValueOnce('');

    // input table
    spyCreateTable.mockResolvedValueOnce('');
    // output table
    spyCreateTable.mockResolvedValueOnce('');
    spyWriteGenFile.mockResolvedValue();

    spyWriteFile.mockResolvedValueOnce();
    // act
    await writeDoc.execute(conf, path);
    //assert

    expect(spyGetRequests).toHaveBeenCalledTimes(1);
    expect(spyMkdir).toHaveBeenCalledTimes(2);
    expect(spyWrite).toHaveBeenCalledTimes(2);
    expect(spyWrite).toHaveBeenNthCalledWith(1, 'test/test', [filesDataWithError], conf);
    expect(spyWrite).toHaveBeenNthCalledWith(2, 'test/pages', [], conf);
    expect(spyWriteFile).toHaveBeenCalledTimes(1);
  });
});
