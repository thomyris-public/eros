import { promises } from 'fs';

import { WriteTableOfContent } from './writeTableOfContent';

describe('WriteTableOfContent', () => {
  const writeToc = new WriteTableOfContent();

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  const tocString = `* [Input](#input)
  * [TestType](#testtype-optionable-false)
* [Output](#output)
  * [TestModel](#testmodel-optionable-false)
* [Errors](#errors)
* [Example](#example)
  * [Request](#request)
  * [Response](#response)
`;

  it('Should return a table of content for a given file string', async () => {
    // arrange
    const fileString = await promises.readFile(__dirname + '/fileString.test', 'utf8');
    // act
    const result = writeToc.execute(fileString);
    // assert
    expect(result).toStrictEqual(tocString);
  });
});
