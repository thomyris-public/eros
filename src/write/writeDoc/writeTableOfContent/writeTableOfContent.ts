export interface WriteTableOfContentInterface {
  execute(fileString: string): string;
}

export class WriteTableOfContent implements WriteTableOfContentInterface {
  execute(fileString: string): string {
    const splitedString = fileString.split(/\n/);

    let tocString = '';
    for (const string of splitedString) {
      if (string[0] === '#') {
        const indentation = string.split(' ')[0].length;
        if (indentation > 1) {
          const indentationString = ' '.repeat((indentation - 2) * 2);
          const formatedString = string.split(/\(/)[0].replace(/\#/g, '').replace(/\[/, '').replace(/\]/, '').trim();
          let stringEnd: string = string.split(/\)/).length > 1 ? string.split(/\)/)[1] : '';

          stringEnd = stringEnd
            .replace(/( (`|:| )*)|(`( |:|`)*)|(:( |`|:)*)/g, '-')
            .replace(/\[|]/g, '')
            .trim();

          const reverseArrayEnd: string[] = stringEnd.split('').reverse();

          // remove '-' at the string's end
          while (reverseArrayEnd[0] === '-') {
            reverseArrayEnd.shift();
          }
          stringEnd = reverseArrayEnd.reverse().join('');

          const link = (formatedString + stringEnd).toLowerCase();
          tocString += `${indentationString}* [${formatedString.split('`')[0]}](#${link})\n`;
        }
      }
    }
    return tocString;
  }
}
