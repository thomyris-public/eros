import { ConfInterface } from '../../helpers/getJsonConfig/conf.interface';
import { FilesDataInterface, RequestDataInterface } from '../../helpers/getApiRequests/requestData.interface';

const conf: ConfInterface = {
  app: {
    title: 'title',
  },
  maxDepth: 2,
  input: '',
  output: {
    path: './',
    dirName: 'test',
  },
};

const emptyRequestData: RequestDataInterface = {
  folders: [],
  rest: [],
};

const filesData: FilesDataInterface = {
  name: 'CreateUser',
  description: 'create a user',
  errors: [],
  inputName: 'input',
  input: null,
  output: null,
  requestType: 'Query',
};

const filesDataWithError: FilesDataInterface = {
  name: 'CreateUser',
  description: '',
  errors: [
    {
      name: 'Error',
      reason: 'test',
    },
    {
      name: 'Error',
    },
  ],
  inputName: 'input',
  input: {
    name: 'CreateUserInput',
    def: [
      {
        default: '"toto"',
        name: 'name',
        type: 'UserName!',
        description: 'Username',
        def: [],
      },
      {
        default: '',
        name: 'password',
        type: 'Password!',
        description: undefined,
        def: [],
      },
    ],
    optionable: false,
  },
  output: {
    name: 'UserInterface',
    def: [
      {
        name: 'name',
        type: 'UserName!',
        description: 'Username',
        def: [],
      },
    ],
    implementations: [],
    optionable: false,
    isArray: false,
  },
  requestType: 'Query',
};

const requestsData: RequestDataInterface = {
  folders: [
    {
      files: [filesData],
      name: 'test',
    },
  ],
  rest: [filesData],
};

const requestsDataError: RequestDataInterface = {
  folders: [
    {
      files: [filesDataWithError],
      name: 'test',
    },
  ],
  rest: [],
};

export { conf, emptyRequestData, requestsData, filesData, filesDataWithError, requestsDataError };
