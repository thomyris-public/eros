import { CreateTable } from './createTable';
import { FormatPath } from '../../../helpers/formatPath/formatPath';
import { GetJsonConfig } from '../../../helpers/getJsonConfig/getJsonConfig';
import { CreateLinkToAssets } from '../../../helpers/createLinkToAssets/createLinkToAssets';
import {
  input1String,
  input2String,
  inputLgthString,
  inputObject1,
  inputObject2,
  inputObjectLgth,
  inputString,
  outputObject1,
  outputObject1String,
  stringInputString,
} from './createTable.test.data';

describe('CreateTable', () => {
  const getConf = new GetJsonConfig();
  const formatPath = new FormatPath();
  const createLink = new CreateLinkToAssets(getConf, formatPath);
  const createTable = new CreateTable(createLink);
  const spyLink = jest.spyOn(createLink, 'execute');
  const path = './test';

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should return a string for a string', async () => {
    // arrange
    spyLink.mockResolvedValueOnce('test');
    spyLink.mockResolvedValueOnce('test');
    // act
    const result = await createTable.execute(inputString, 'Input', path);
    // assert
    expect(result).toStrictEqual(stringInputString);
  });

  it('Should return a string for an inputData', async () => {
    // arrange
    spyLink.mockResolvedValueOnce('test');
    spyLink.mockResolvedValueOnce('test');
    // act
    const result = await createTable.execute(inputObject1, 'Input', path);
    // assert
    expect(result).toStrictEqual(input1String);
  });

  it('Should return a string for an outputData containing subOutput', async () => {
    // arrange
    spyLink.mockResolvedValueOnce('test');
    spyLink.mockResolvedValueOnce('test');
    // act
    const result = await createTable.execute(outputObject1, 'Output', path);
    // assert
    expect(result).toStrictEqual(outputObject1String);
  });

  it('Should convert lgth fields to length', async () => {
    // arrange
    spyLink.mockResolvedValueOnce('test');
    spyLink.mockResolvedValueOnce('test');
    // act
    const result = await createTable.execute(inputObjectLgth, 'Input', path);
    // assert
    expect(result).toStrictEqual(inputLgthString);
  });

  it('Should return a string for an inputData with deprecated', async () => {
    // arrange
    spyLink.mockResolvedValueOnce('test');
    spyLink.mockResolvedValueOnce('test');
    // act
    const result = await createTable.execute(inputObject2, 'Input', path);
    // assert
    expect(result).toStrictEqual(input2String);
  });
});
