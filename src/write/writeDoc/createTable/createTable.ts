import { CreateLinkToAssetsInterface } from '../../../helpers/createLinkToAssets/createLinkToAssets';

export interface CreateTableInterface {
  execute(object: any, type: string, path: string): Promise<string>;
}

export class CreateTable {
  private createLinkToAssets: CreateLinkToAssetsInterface;

  constructor(createLink: CreateLinkToAssetsInterface) {
    this.createLinkToAssets = createLink;
  }

  async execute(object: string | any, type: string, path: string): Promise<string> {
    interface ObjectTable {
      fields: string[];
      types: string[];
      optionable: boolean[];
      default: string[];
      descriptions: string[];
      deprecated: string[];
    }

    const objectTable: ObjectTable = {
      fields: [],
      types: [],
      optionable: [],
      default: [],
      descriptions: [],
      deprecated: [],
    };

    // if object don't have a def property return string containing the object
    if (typeof object === 'string') {
      const tableString = `The ${type.toLowerCase()} is a **${object.split('!')[0]}**`;
      return tableString;
    }

    // for each properties in the object definition
    for (const prop of object.def) {
      const optionable: boolean = prop.type[prop.type.length - 1] !== '!';
      const description: string = prop.description ?? '';
      const deprecated: string = prop.deprecated ?? '';

      let name: string = prop.name;
      if (name === 'lgth') name = 'length';

      // list in arrays the obj properties at same index
      objectTable.fields.push(name);
      objectTable.types.push(prop.type.split('!').join(''));
      objectTable.default.push(prop.default);
      objectTable.optionable.push(optionable);
      objectTable.descriptions.push(description);
      objectTable.deprecated.push(deprecated);
    }

    // construct table's headers
    let headersTable = '| ';
    let subHeaderTable = '| ';

    for (const tableTitle in objectTable) {
      let tableName = tableTitle;

      // construct the table header
      if (type !== 'Input' && tableTitle === 'default') continue;
      if (type === 'Output' && tableTitle === 'optionable') tableName = 'nullable';
      headersTable += `${tableName} |`;

      // construct table header separator
      for (let i = 0; i < tableTitle.length; i++) {
        if (i === 0) subHeaderTable += ':';
        else if (i === tableTitle.length - 1) subHeaderTable += ':';
        else subHeaderTable += '-';
      }

      subHeaderTable += '  |';
    }
    headersTable += '\n';
    subHeaderTable += '\n';

    let contentString = '';
    for (let i = 0; i < objectTable.fields.length; i++) {
      let row = '| ';
      row += `${objectTable.fields[i]} |`;
      const linkType = await this.createLinkToAssets.execute(objectTable.types[i].trim(), path);
      row += `${linkType} |`;
      row += `${objectTable.optionable[i]} |`;
      if (type === 'Input') row += `${objectTable.default[i]} |`;
      row += objectTable.descriptions[i] ? `${objectTable.descriptions[i]} |` : ' |';
      row += objectTable.deprecated[i] ? `${objectTable.deprecated[i]} |` : ' |';
      row += '\n';
      contentString += row;
    }

    // construct sub table for the complex sub type
    const subTables = await this.writeSubTable(object, type, path);

    // add [] if the type is an array
    let objectName = object.isArray ? `[${object.name}]` : object.name;

    // add link to the asset file if it exist
    objectName = await this.createLinkToAssets.execute(objectName, path);

    const tableString = `### ${objectName} \`optionable: ${object.optionable}\`\n` + headersTable + subHeaderTable + contentString + '\n' + subTables;
    return tableString;
  }

  private async writeSubTable(object: any, type: string, path: string) {
    let string = '';

    for (let i = 0; i < object.def.length; i++) {
      const def = object.def[i];
      if (def.def.length > 0) {
        for (let j = 0; j < def.def.length; j++) {
          const subDef = def.def[j];
          string += await this.execute(subDef, type, path);
        }
      }
    }

    return string;
  }
}
