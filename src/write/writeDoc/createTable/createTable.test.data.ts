import { InputDataInterface, OutputDataInterface } from '../../../helpers/getApiRequests/requestData.interface';

const inputString = 'test';

const stringInputString = 'The input is a **test**';

const inputObject1: InputDataInterface = {
  def: [
    {
      default: '',
      name: 'test',
      type: 'string',
      description: 'Desc',
      def: [],
    },
  ],
  name: 'inputTest1',
  optionable: false,
};

let input1String = '### test `optionable: false`\n| fields |types |optionable |default |descriptions |deprecated |\n';
input1String += '| :----:  |:---:  |:--------:  |:-----:  |:----------:  |:--------:  |\n| test |test |true | |Desc | |\n\n';

const outputObject1: OutputDataInterface = {
  def: [
    {
      name: 'test',
      type: 'string',
      description: 'Desc',
      def: [
        {
          name: 'subOutput',
          optionable: false,
          isArray: false,
          def: [
            {
              name: 'subOutputDef',
              type: 'string',
              description: undefined,
              def: [],
            },
          ],
          implementations: [],
        },
      ],
    },
  ],
  implementations: [],
  isArray: true,
  name: 'outputTest1',
  optionable: false,
};

let outputObject1String = '### test `optionable: false`\n| fields |types |nullable |descriptions |deprecated |\n';
outputObject1String += '| :----:  |:---:  |:--------:  |:----------:  |:--------:  |\n| test |test |true |Desc | |\n';
// add subOutput string
outputObject1String += '\n';
outputObject1String +=
  '### test `optionable: false`\n| fields |types |nullable |descriptions |deprecated |\n| :----:  |:---:  |:--------:  |:----------:  |:--------:  |\n| subOutputDef |test |true | | |\n';
outputObject1String += '\n';

const inputObjectLgth: InputDataInterface = {
  def: [
    {
      default: '',
      name: 'lgth',
      type: 'string',
      description: 'Desc',
      deprecated: undefined,
      def: [],
    },
  ],
  name: 'inputTest1',
  optionable: false,
};

let inputLgthString = '### test `optionable: false`\n| fields |types |optionable |default |descriptions |deprecated |\n';
inputLgthString += '| :----:  |:---:  |:--------:  |:-----:  |:----------:  |:--------:  |\n| length |test |true | |Desc | |\n\n';

const inputObject2: InputDataInterface = {
  def: [
    {
      default: '',
      name: 'test',
      type: 'string',
      description: 'Desc',
      deprecated: 'do not use this',
      def: [],
    },
  ],
  name: 'inputTest1',
  optionable: false,
};

let input2String = '### test `optionable: false`\n| fields |types |optionable |default |descriptions |deprecated |\n';
input2String += '| :----:  |:---:  |:--------:  |:-----:  |:----------:  |:--------:  |\n| test |test |true | |Desc |do not use this |\n\n';

export { inputString, stringInputString, inputObject1, input1String, outputObject1, outputObject1String, inputObjectLgth, inputLgthString, inputObject2, input2String };
