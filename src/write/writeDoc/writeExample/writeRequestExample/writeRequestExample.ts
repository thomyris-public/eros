import { RequestExampleInterface } from './requestExample.interface';
import { ConfInterface } from '../../../../helpers/getJsonConfig/conf.interface';
import { GetIndentationInterface } from '../../../../helpers/getIndentation/getIndentation';
import { GenerateFromTypeInterface } from '../../../../helpers/generateFromType/generateFromType';
import { FilesDataInterface, ImplementationsDataInterface } from '../../../../helpers/getApiRequests/requestData.interface';

export interface WriteRequestExampleInterface {
  execute(request: FilesDataInterface, conf: ConfInterface): Promise<RequestExampleInterface>;
}

export class WriteRequestExample implements WriteRequestExampleInterface {
  private getIndentation: GetIndentationInterface;
  private generateFromType: GenerateFromTypeInterface;

  constructor(getIdentation: GetIndentationInterface, generateFromType: GenerateFromTypeInterface) {
    this.getIndentation = getIdentation;
    this.generateFromType = generateFromType;
  }

  async execute(request: FilesDataInterface, conf: ConfInterface): Promise<RequestExampleInterface> {
    let exampleString = `### Request\n\`\`\`graphql\n${request.requestType} {\n  ${request.name}`;
    let generatedInput: string[] = [];

    // write inputs
    if (request.input) {
      exampleString += ` (\n    ${request.inputName}: {`;
      const generatedRequest = await this.writeString(request.input, 3, conf);
      generatedInput = generatedRequest.generated;
      const inputString = generatedRequest.string;
      exampleString += inputString;
      exampleString += '\n  )';
    }

    if (typeof request.output === 'string') {
      // don't open an object if the output is not an object
    } else exampleString += ' {'; // open the object

    // write outputs
    if (request.output && typeof request.output !== 'string' && request.output.def) {
      const generatedRequest = await this.writeString(request.output, 2, conf, 'output');
      const outputString = generatedRequest.string;
      exampleString += outputString;
    }

    exampleString += '\n}\n```';
    const returnObject: RequestExampleInterface = {
      string: exampleString,
      generated: generatedInput,
    };

    return returnObject;
  }

  private async writeString(object: any, tabNbr: number, conf: ConfInterface, objectType = 'input', secondCall = false): Promise<RequestExampleInterface> {
    const returnObject: RequestExampleInterface = {
      string: '',
      generated: [],
    };

    // loop on each object's definitions
    for (let i = 0; i < object.def.length; i++) {
      const prop = object.def[i];

      if (prop.def.length > 0) {
        // generate the string for the sub object

        for (let j = 0; j < prop.def.length; j++) {
          const subProp = prop.def[j];
          const generatedRequest: RequestExampleInterface = await this.writeString(subProp, tabNbr + 1, conf, objectType, true);
          let subString = generatedRequest.string;

          // generate the string for the implementations
          const implString = await this.writeImplString(subProp.implementations, tabNbr + 1, conf, objectType);

          subString = subString.slice(0, subString.lastIndexOf('\n'));

          if (implString !== '') subString += implString;
          const inputsep = objectType === 'input' ? ':' : '';

          if (subString === '') subString = `\n${this.getIndentation.execute(tabNbr + 1)}...`;
          returnObject.string += `\n${this.getIndentation.execute(tabNbr)}${prop.name}${inputsep} {${subString}\n${this.getIndentation.execute(tabNbr)}}`;
        }
      } else {
        let name: string = prop.name;
        if (name === 'lgth') name = 'length';

        // generate for the input part of the request
        if (objectType === 'input') {
          let inputValue = await this.generateFromType.execute(prop.type, conf);

          const generatedProp = {
            name: name,
            generatedString: inputValue,
          };

          returnObject.generated.push(generatedProp);

          // generate for an array
          const isArray = /\[.+\]/.test(prop.type);
          if (isArray) {
            inputValue = `[\n${this.getIndentation.execute(tabNbr + 1)}${inputValue},\n${this.getIndentation.execute(tabNbr + 1)}${inputValue}\n${this.getIndentation.execute(
              tabNbr,
            )}]`;
          }
          returnObject.string += `\n${this.getIndentation.execute(tabNbr)}${name}: ${inputValue}`;
        } else returnObject.string += `\n${this.getIndentation.execute(tabNbr)}${name}`;
      }
    }

    if (!secondCall && object.implementations) {
      // generate the impl string for input object if it has implementations
      const implString = await this.writeImplString(object.implementations, tabNbr, conf, objectType);
      returnObject.string += implString;
    }
    returnObject.string += `\n${this.getIndentation.execute(tabNbr - 1)}}`;
    return returnObject;
  }

  // write the string for an interface's implementations
  private async writeImplString(implementations: ImplementationsDataInterface[], tabNbr: number, conf: ConfInterface, objectType: string) {
    let string = '';
    // is there implementations
    if (implementations && implementations.length > 0) {
      for (let i = 0; i < implementations.length; i++) {
        const implementation = implementations[i];
        // is there a definition
        if (implementation.def.length > 0) {
          string += `\n${this.getIndentation.execute(tabNbr)}... on ${implementation.name} {`;
          const implString = await this.writeString(implementation, tabNbr + 1, conf, objectType, true);
          string += implString.string;
        }
      }
      return string;
    }
    return string;
  }
}
