import { WriteRequestExample } from './writeRequestExample';
import { GenFileHandler } from '../../../../helpers/genFileHandler/genFileHandler';
import { GetIndentation } from '../../../../helpers/getIndentation/getIndentation';
import { GenerateFromType } from '../../../../helpers/generateFromType/generateFromType';
import {
  conf,
  requestInput,
  requestInputExample,
  requestNoInput,
  requestNoInputExample,
  requestOutput,
  requestOutputExample,
  requestOutputExampleLgth,
  requestOutputImpl,
  requestOutputImplExample,
  requestOutputLgth,
  requestOutputString,
  requestOutputStringExample,
  requestOutputSub,
  requestOutputSubExample,
} from './writeRequestExample.test.data';

describe('WriteRequestExample', () => {
  const getIndentation = new GetIndentation();
  const handleGenFile = new GenFileHandler();
  const generate = new GenerateFromType(handleGenFile);
  const writeRequest = new WriteRequestExample(getIndentation, generate);

  const spyWriteGenFile = jest.spyOn(handleGenFile, 'write');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should return a string for a request with an input def and an implementation', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await writeRequest.execute(requestInput, conf);
    // assert
    expect(result).toStrictEqual(requestInputExample);
  });

  it('Should return a string for a request without Input', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await writeRequest.execute(requestNoInput, conf);
    // assert
    expect(result).toStrictEqual(requestNoInputExample);
  });

  it('Should return a string for a request with an output that have a subOutput', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await writeRequest.execute(requestOutputSub, conf);
    // assert
    expect(result).toStrictEqual(requestOutputSubExample);
  });

  it('Should return a string for a request with an output that have sub', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await writeRequest.execute(requestOutput, conf);
    // assert
    expect(result).toStrictEqual(requestOutputExample);
  });

  it('Should return a string for a request with an output that have a subOutput and have an implementation', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await writeRequest.execute(requestOutputImpl, conf);
    // assert
    expect(result).toStrictEqual(requestOutputImplExample);
  });

  it('Should return a string for a request with a string as output', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await writeRequest.execute(requestOutputString, conf);
    // assert
    expect(result).toStrictEqual(requestOutputStringExample);
  });

  it('Should convert lgth to length', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await writeRequest.execute(requestOutputLgth, conf);
    // assert
    expect(result).toStrictEqual(requestOutputExampleLgth);
  });
});
