import { RequestExampleInterface } from './requestExample.interface';
import { ConfInterface } from '../../../../helpers/getJsonConfig/conf.interface';
import { FilesDataInterface, InputDataInterface, OutputDataInterface } from '../../../../helpers/getApiRequests/requestData.interface';

const conf: ConfInterface = {
  input: 'input',
  maxDepth: 2,
  output: {
    path: './',
    dirName: '',
  },
  app: {
    title: 'app',
    description: 'description',
  },
  scalarDefinitions: [
    {
      name: 'test',
      regex: '^abc$',
    },
  ],
};

const requestInput: FilesDataInterface = {
  description: '',
  errors: [],
  input: {
    def: [
      {
        default: '',
        name: 'test',
        type: '[testType]',
        description: 'Comment',
        def: [],
      },
      {
        default: '',
        name: 'noArray',
        type: 'noArrayType',
        def: [],
      },
    ],
    name: 'input',
    optionable: false,
  },
  inputName: 'input',
  name: 'testRequest',
  output: {
    def: [],
    implementations: [],
    isArray: false,
    name: 'output',
    optionable: false,
  },
  requestType: 'Query',
};

const subInput: InputDataInterface = {
  def: [
    {
      default: '',
      name: 'testSub',
      type: '[testType]',
      description: 'Comment',
      def: [],
    },
  ],
  name: 'testType',
  optionable: true,
};

requestInput.input!.def[0].def = [subInput];

const requestInputExample: RequestExampleInterface = {
  generated: [
    {
      generatedString: 'noArrayType',
      name: 'noArray',
    },
  ],
  string:
    '### Request\n```graphql\nQuery {\n  testRequest (\n    input: {\n      test: {\n        testSub: [\n          testType,\n          testType\n        ]\n      }\n      noArray: noArrayType\n    }\n  ) {\n  }\n}\n```',
};

const requestNoInput: FilesDataInterface = {
  description: '',
  errors: [],
  input: null,
  inputName: 'input',
  name: 'testRequest',
  output: {
    def: [],
    implementations: [],
    isArray: false,
    name: 'output',
    optionable: false,
  },
  requestType: 'Query',
};

const requestNoInputExample: RequestExampleInterface = {
  generated: [],
  string: '### Request\n```graphql\nQuery {\n  testRequest {\n  }\n}\n```',
};

const requestOutput: FilesDataInterface = {
  description: '',
  errors: [],
  input: null,
  inputName: 'input',
  name: 'testRequest',
  output: {
    def: [
      {
        name: 'test',
        type: 'testType',
        description: 'Comment',
        def: [],
      },
    ],
    implementations: [],
    isArray: false,
    name: 'output',
    optionable: false,
  },
  requestType: 'Query',
};

const subOutputImpl: OutputDataInterface = {
  name: 'testImpl',
  isArray: false,
  optionable: false,
  def: [
    {
      name: 'name',
      type: 'nameType',
      def: [],
    },
  ],
  implementations: [
    {
      def: [
        {
          name: 'implDef',
          type: 'testType',
          def: [],
        },
      ],
      name: 'implInSub',
    },
  ],
};

const subOutput: OutputDataInterface = {
  name: 'testImpl',
  isArray: false,
  optionable: false,
  def: [
    {
      name: 'name',
      type: 'nameType',
      def: [],
    },
  ],
  implementations: [
    {
      def: [],
      name: 'implInImpl',
    },
  ],
};

const requestOutputSub = {
  description: '',
  errors: [],
  input: null,
  inputName: 'input',
  name: 'testRequest',
  output: {
    def: [
      {
        name: 'test',
        type: 'testType',
        description: 'Comment',
        def: [subOutput],
      },
    ],
    implementations: [],
    isArray: false,
    name: 'output',
    optionable: false,
  },
  requestType: 'Query',
};

const requestOutputImpl = {
  description: '',
  errors: [],
  input: null,
  inputName: 'input',
  name: 'testRequest',
  output: {
    def: [
      {
        name: 'test',
        type: 'testType',
        description: 'Comment',
        def: [subOutputImpl],
      },
    ],
    implementations: [],
    isArray: false,
    name: 'output',
    optionable: false,
  },
  requestType: 'Query',
};

const requestOutputExample: RequestExampleInterface = {
  generated: [],
  string: '### Request\n```graphql\nQuery {\n  testRequest {\n    test\n  }\n}\n```',
};

const requestOutputImplExample: RequestExampleInterface = {
  generated: [],
  string: '### Request\n```graphql\nQuery {\n  testRequest {\n    test {\n      name\n      ... on implInSub {\n        implDef\n      }\n    }\n  }\n}\n```',
};

const requestOutputSubExample: RequestExampleInterface = {
  generated: [],
  string: '### Request\n```graphql\nQuery {\n  testRequest {\n    test {\n      name\n    }\n  }\n}\n```',
};

const requestOutputString: FilesDataInterface = {
  description: '',
  errors: [],
  input: null,
  inputName: 'input',
  name: 'testRequest',
  output: 'outputString',
  requestType: 'Mutation',
};

const requestOutputStringExample: RequestExampleInterface = {
  generated: [],
  string: '### Request\n```graphql\nMutation {\n  testRequest\n}\n```',
};

const requestOutputLgth: FilesDataInterface = {
  description: '',
  errors: [],
  input: null,
  inputName: 'input',
  name: 'testRequest',
  output: {
    def: [
      {
        name: 'lgth',
        type: 'testType',
        description: 'Comment',
        def: [],
      },
    ],
    implementations: [],
    isArray: false,
    name: 'output',
    optionable: false,
  },
  requestType: 'Query',
};

const requestOutputExampleLgth: RequestExampleInterface = {
  generated: [],
  string: '### Request\n```graphql\nQuery {\n  testRequest {\n    length\n  }\n}\n```',
};

export {
  conf,
  requestInput,
  requestInputExample,
  requestNoInput,
  requestNoInputExample,
  requestOutput,
  requestOutputExample,
  requestOutputString,
  requestOutputStringExample,
  requestOutputImpl,
  requestOutputImplExample,
  requestOutputSub,
  requestOutputSubExample,
  requestOutputLgth,
  requestOutputExampleLgth,
};
