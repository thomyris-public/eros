import { ConfInterface } from '../../../helpers/getJsonConfig/conf.interface';
import { WriteRequestExampleInterface } from './writeRequestExample/writeRequestExample';
import { FilesDataInterface } from '../../../helpers/getApiRequests/requestData.interface';
import { WriteResponseExampleInterface } from './writeResponseExample/writeResponseExample';

export interface WriteExampleInterface {
  execute(request: FilesDataInterface, conf: ConfInterface): Promise<string>;
}

export class WriteExample implements WriteExampleInterface {
  private writeRequest: WriteRequestExampleInterface;
  private writeResponse: WriteResponseExampleInterface;

  constructor(writeRequest: WriteRequestExampleInterface, writeResponse: WriteResponseExampleInterface) {
    this.writeRequest = writeRequest;
    this.writeResponse = writeResponse;
  }

  async execute(request: FilesDataInterface, conf: ConfInterface): Promise<string> {
    const generatedRequest = await this.writeRequest.execute(request, conf);
    const responseString = await this.writeResponse.execute(request, conf, generatedRequest.generated);
    const exampleString = `## Example\n${generatedRequest.string}\n${responseString}`;
    return exampleString;
  }
}
