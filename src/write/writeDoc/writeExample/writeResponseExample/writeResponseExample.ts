import { ConfInterface } from '../../../../helpers/getJsonConfig/conf.interface';
import { GetIndentationInterface } from '../../../../helpers/getIndentation/getIndentation';
import { GenerateFromTypeInterface } from '../../../../helpers/generateFromType/generateFromType';
import { FilesDataInterface, ImplementationsDataInterface } from '../../../../helpers/getApiRequests/requestData.interface';

export interface WriteResponseExampleInterface {
  execute(request: FilesDataInterface, conf: ConfInterface, generated: any[]): Promise<string>;
}

export class WriteResponseExample implements WriteResponseExampleInterface {
  private getIndentation: GetIndentationInterface;
  private generateFromType: GenerateFromTypeInterface;

  constructor(getIdentation: GetIndentationInterface, generateFromType: GenerateFromTypeInterface) {
    this.getIndentation = getIdentation;
    this.generateFromType = generateFromType;
  }

  async execute(request: FilesDataInterface, conf: ConfInterface, generated: any[]): Promise<string> {
    let responseString = `### Response\n\`\`\`json\n{\n${this.getIndentation.execute(1)}"data": {\n${this.getIndentation.execute(2)}"${request.name}":`;
    let tabNbr = 3;

    if (typeof request.output === 'string') responseString += ` ${await this.generateFromType.execute(request.output, conf)}`;
    // use array synthax if is an array
    else if (request.output && request.output.isArray) {
      tabNbr++;
      responseString += ` [\n${this.getIndentation.execute(3)}{`;
    } else {
      responseString += ' {';
    }

    // if have a definition create the string for it
    if (request.output && typeof request.output !== 'string' && request.output.def) {
      const outputString: string = await this.writeString(request.output, tabNbr, conf, generated);
      responseString += `${outputString}\n${this.getIndentation.execute(tabNbr - 1)}}`;
    }

    // close string for array
    if (request.output && typeof request.output !== 'string' && request.output.isArray) responseString += `\n${this.getIndentation.execute(2)}]`;

    responseString += `\n${this.getIndentation.execute(1)}}\n}\n\`\`\``;
    return responseString;
  }

  private async writeString(request: any, tabNbr: number, conf: ConfInterface, generated: any[], secondCall = false): Promise<string> {
    let string = '';

    for (let i = 0; i < request.def.length; i++) {
      const prop = request.def[i];
      const type = prop.type;
      const coma = i !== request.def.length - 1 ? ',' : '';

      if (prop.def.length > 0) {
        for (let i = 0; i < prop.def.length; i++) {
          const subProp = prop.def[i];
          let subString: string = await this.writeString(subProp, tabNbr + 1, conf, generated, true);
          const implString: string = await this.writeImplString(subProp.implementations, tabNbr + 1, conf, generated);

          if (implString) {
            subString += implString;
          }

          if (subString === '') subString = `\n${this.getIndentation.execute(tabNbr + 1)}...`;
          const openArray = subProp.isArray ? `[\n${this.getIndentation.execute(tabNbr)}` : '';
          const closeArray = subProp.isArray ? `\n${this.getIndentation.execute(tabNbr)}]` : '';
          string += `\n${this.getIndentation.execute(tabNbr)}"${prop.name}": ${openArray}{${subString}\n${this.getIndentation.execute(tabNbr)}}${closeArray}${coma}`;
        }
      } else {
        let isAlreadyGenerated: any;

        let name: string = prop.name;
        if (name === 'lgth') name = 'length';

        if (generated.length > 0) isAlreadyGenerated = generated.find((elmt) => elmt.name === name);

        let propValue: string | number | boolean;

        // get the value generated in the request's input example
        if (isAlreadyGenerated) {
          propValue = isAlreadyGenerated.generatedString;
        } else {
          propValue = await this.generateFromType.execute(type, conf);
        }

        // generate for an array
        const isArray: boolean = /\[.+\]/.test(type);
        if (isArray) {
          propValue = `[\n${this.getIndentation.execute(tabNbr + 1)}${propValue},\n${this.getIndentation.execute(tabNbr + 1)}${propValue}\n${this.getIndentation.execute(tabNbr)}]`;
        }

        string += `\n${this.getIndentation.execute(tabNbr)}"${name}": ${propValue}${coma}`;
      }
    }

    if (!secondCall) {
      const implString = await this.writeImplString(request.implementations, tabNbr, conf, generated);
      string += implString;
    }

    return string;
  }

  private async writeImplString(implementations: ImplementationsDataInterface[], tabNbr: number, conf: ConfInterface, generated: any[]): Promise<string> {
    let string = '';

    if (implementations && implementations.length > 0) {
      const implementation = implementations[0];
      const implString = await this.writeString(implementation, tabNbr, conf, generated, true);
      string += implString;
      return string;
    }
    return string;
  }
}
