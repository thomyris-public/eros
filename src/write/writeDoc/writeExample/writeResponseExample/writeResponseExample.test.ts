import { WriteResponseExample } from './writeResponseExample';
import { GenFileHandler } from '../../../../helpers/genFileHandler/genFileHandler';
import { GetIndentation } from '../../../../helpers/getIndentation/getIndentation';
import { GenerateFromType } from '../../../../helpers/generateFromType/generateFromType';
import {
  conf,
  queryString,
  queryObject,
  queryWithSubObject,
  queryWithSubString,
  queryWithImplObject,
  queryWithImplString,
  queryWithArrayObject,
  queryWithArrayString,
  queryNoDefObject,
  queryNoDefString,
  generated,
  queryWithSubOutImplObject,
  queryWithSubOutImplString,
  queryWithArrayObjectLgth,
  queryWithArrayStringLgth,
} from './writeResponseExample.test.data';

describe('WriteResponseExample', () => {
  const getIndentation = new GetIndentation();
  const genFileHandler = new GenFileHandler();
  const generateFromType = new GenerateFromType(genFileHandler);
  const writeResponse = new WriteResponseExample(getIndentation, generateFromType);

  const spyWriteGenFile = jest.spyOn(genFileHandler, 'write');
  const spyGenerate = jest.spyOn(generateFromType, 'execute');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  it('Should return the response string for a query', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    spyGenerate.mockResolvedValueOnce('UserName').mockResolvedValueOnce('"test"');
    // act
    const result = await writeResponse.execute(queryObject, conf, []);
    // assert
    expect(result).toStrictEqual(queryString);
  });

  it('Should return the response string for a query with subOutputDef with already generated value', async () => {
    // arrange
    // act
    const result = await writeResponse.execute(queryWithSubObject, conf, generated);
    // assert
    expect(result).toStrictEqual(queryWithSubString);
  });

  it('Should return the response string for a query with an output implementation', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    spyGenerate.mockResolvedValueOnce('UserName').mockResolvedValueOnce('ImplType');
    // act
    const result = await writeResponse.execute(queryWithImplObject, conf, []);
    // assert
    expect(result).toStrictEqual(queryWithImplString);
  });

  it('Should return the response string for a query with a subOutput implementation', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    spyGenerate.mockResolvedValueOnce('sub').mockResolvedValueOnce('ImplType');
    // act
    const result = await writeResponse.execute(queryWithSubOutImplObject, conf, []);
    // assert
    expect(result).toStrictEqual(queryWithSubOutImplString);
  });

  it('Should return the response string for a query with an arrays as output', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    spyGenerate.mockResolvedValueOnce('UserName').mockResolvedValueOnce('UserName');
    // act
    const result = await writeResponse.execute(queryWithArrayObject, conf, []);
    // assert
    expect(result).toStrictEqual(queryWithArrayString);
  });

  it('Should return the response string for a query with a string as output (should not happen)', async () => {
    // arrange
    spyGenerate.mockResolvedValueOnce('outputNoDef');
    // act
    const result = await writeResponse.execute(queryNoDefObject, conf, []);
    // assert
    expect(result).toStrictEqual(queryNoDefString);
  });

  it('Should convert lgth to length', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    spyGenerate.mockResolvedValueOnce('UserName').mockResolvedValueOnce('length');
    // act
    const result = await writeResponse.execute(queryWithArrayObjectLgth, conf, []);
    // assert
    expect(result).toStrictEqual(queryWithArrayStringLgth);
  });
});
