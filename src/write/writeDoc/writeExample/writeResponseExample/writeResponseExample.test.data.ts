import { ConfInterface } from '../../../../helpers/getJsonConfig/conf.interface';
import { FilesDataInterface, ImplementationsDataInterface, OutputDataInterface } from '../../../../helpers/getApiRequests/requestData.interface';

const conf: ConfInterface = {
  input: 'input',
  maxDepth: 2,
  output: {
    path: './',
    dirName: '',
  },
  app: {
    title: 'app',
    description: 'description',
  },
  scalarDefinitions: [
    {
      name: 'test',
      regex: '^abc$',
    },
  ],
};

const output: OutputDataInterface = {
  name: 'UserType',
  def: [
    {
      name: 'name',
      type: '[UserName!]!',
      description: 'Username',
      def: [],
    },
    {
      name: 'test',
      type: 'String!',
      description: '',
      def: [],
    },
  ],
  implementations: [],
  optionable: false,
  isArray: true,
};

const queryObject: FilesDataInterface = {
  description: 'Get users',
  errors: [],
  input: null,
  inputName: null,
  name: 'getUsers',
  output: output,
  requestType: 'query',
};

const queryString =
  '### Response\n```json\n{\n  "data": {\n    "getUsers": [\n      {\n        "name": [\n          UserName,\n          UserName\n        ],\n        "test": "test"\n      }\n    ]\n  }\n}\n```';

const subOutput: OutputDataInterface = {
  name: 'name',
  def: [
    {
      name: 'sub',
      type: 'sub!',
      description: '',
      def: [],
    },
  ],
  implementations: [],
  optionable: false,
  isArray: false,
};

const outputWithSub: OutputDataInterface = {
  name: 'UserType',
  def: [
    {
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      def: [],
    },
  ],
  implementations: [],
  optionable: false,
  isArray: false,
};

outputWithSub.def[0].def = [subOutput];

const queryWithSubObject: FilesDataInterface = {
  description: 'Get users',
  errors: [],
  input: null,
  inputName: null,
  name: 'getUsers',
  output: outputWithSub,
  requestType: 'query',
};

const generated = [
  {
    generatedString: 'generated sub',
    name: 'sub',
  },
];

const generatedNoMatch = [
  {
    generatedString: 'no match',
    name: 'noMatch',
  },
];

const queryWithSubString = '### Response\n```json\n{\n  "data": {\n    "getUsers": {\n      "name": {\n        "sub": generated sub\n      }\n    }\n  }\n}\n```';

const impl: ImplementationsDataInterface = {
  def: [
    {
      name: 'impl',
      type: 'ImplType!',
      description: '',
      def: [],
    },
  ],
  name: 'implType',
};

const outputWithImpl: OutputDataInterface = {
  name: 'UserType',
  def: [
    {
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      def: [],
    },
  ],
  implementations: [impl],
  optionable: false,
  isArray: false,
};

const queryWithImplObject: FilesDataInterface = {
  description: 'Get users',
  errors: [],
  input: null,
  inputName: null,
  name: 'getUsers',
  output: outputWithImpl,
  requestType: 'query',
};

const queryWithImplString = '### Response\n```json\n{\n  "data": {\n    "getUsers": {\n      "name": UserName\n      "impl": ImplType\n    }\n  }\n}\n```';

const subOutputWithImp: OutputDataInterface = {
  name: 'name',
  def: [
    {
      name: 'sub',
      type: 'sub!',
      description: '',
      def: [],
    },
  ],
  implementations: [impl],
  optionable: false,
  isArray: false,
};

const outputWithSubImpl: OutputDataInterface = {
  name: 'UserType',
  def: [
    {
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      def: [],
    },
  ],
  implementations: [],
  optionable: false,
  isArray: false,
};

outputWithSubImpl.def[0].def = [subOutputWithImp];

const queryWithSubOutImplObject: FilesDataInterface = {
  description: 'Get users',
  errors: [],
  input: null,
  inputName: null,
  name: 'getUsers',
  output: outputWithSubImpl,
  requestType: 'query',
};

const queryWithSubOutImplString =
  '### Response\n```json\n{\n  "data": {\n    "getUsers": {\n      "name": {\n        "sub": sub\n        "impl": ImplType\n      }\n    }\n  }\n}\n```';

const outputArray: OutputDataInterface = {
  name: 'UserType',
  def: [
    {
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      def: [],
    },
    {
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      def: [],
    },
  ],
  implementations: [],
  optionable: true,
  isArray: true,
};

const queryWithArrayObject: FilesDataInterface = {
  description: 'Get users',
  errors: [],
  input: null,
  inputName: null,
  name: 'getUsers',
  output: outputArray,
  requestType: 'query',
};

const queryWithArrayString = '### Response\n```json\n{\n  "data": {\n    "getUsers": [\n      {\n        "name": UserName,\n        "name": UserName\n      }\n    ]\n  }\n}\n```';

const queryNoDefObject: FilesDataInterface = {
  description: 'Get users',
  errors: [],
  input: null,
  inputName: null,
  name: 'getUsers',
  output: 'outputNoDef',
  requestType: 'query',
};

const queryNoDefString = '### Response\n```json\n{\n  "data": {\n    "getUsers": outputNoDef\n  }\n}\n```';

const outputArrayLgth: OutputDataInterface = {
  name: 'UserType',
  def: [
    {
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      def: [],
    },
    {
      name: 'lgth',
      type: 'length',
      description: undefined,
      def: [],
    },
  ],
  implementations: [],
  optionable: true,
  isArray: true,
};

const queryWithArrayObjectLgth: FilesDataInterface = {
  description: 'Get users',
  errors: [],
  input: null,
  inputName: null,
  name: 'getUsers',
  output: outputArrayLgth,
  requestType: 'query',
};

const queryWithArrayStringLgth =
  '### Response\n```json\n{\n  "data": {\n    "getUsers": [\n      {\n        "name": UserName,\n        "length": length\n      }\n    ]\n  }\n}\n```';

export {
  conf,
  queryObject,
  queryString,
  queryWithSubObject,
  generated,
  generatedNoMatch,
  queryWithSubString,
  queryWithImplObject,
  queryWithImplString,
  queryWithSubOutImplObject,
  queryWithSubOutImplString,
  queryWithArrayObject,
  queryWithArrayString,
  queryNoDefObject,
  queryNoDefString,
  queryWithArrayObjectLgth,
  queryWithArrayStringLgth,
};
