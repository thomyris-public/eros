import { WriteExample } from './writeExample';
import { conf, request, exampleString } from './writeExample.test.data';
import { GenFileHandler } from '../../../helpers/genFileHandler/genFileHandler';
import { WriteRequestExample } from './writeRequestExample/writeRequestExample';
import { GetIndentation } from '../../../helpers/getIndentation/getIndentation';
import { WriteResponseExample } from './writeResponseExample/writeResponseExample';
import { GenerateFromType } from '../../../helpers/generateFromType/generateFromType';

describe('WriteExample', () => {
  const handleGenFile = new GenFileHandler();
  const getIndentation = new GetIndentation();
  const generateFromType = new GenerateFromType(handleGenFile);
  const writeRequest = new WriteRequestExample(getIndentation, generateFromType);
  const writeResponse = new WriteResponseExample(getIndentation, generateFromType);
  const writeExample = new WriteExample(writeRequest, writeResponse);

  const spyWriteGenFile = jest.spyOn(handleGenFile, 'write');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should return the example string', async () => {
    // arrange
    spyWriteGenFile.mockResolvedValue();
    //act
    const result = await writeExample.execute(request, conf);
    // assert
    expect(result).toStrictEqual(exampleString);
  });
});
