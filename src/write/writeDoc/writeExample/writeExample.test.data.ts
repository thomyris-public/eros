import { ConfInterface } from '../../../helpers/getJsonConfig/conf.interface';
import { FilesDataInterface, InputDataInterface } from '../../../helpers/getApiRequests/requestData.interface';

const conf: ConfInterface = {
  input: 'input',
  maxDepth: 2,
  output: {
    path: './',
    dirName: '',
  },
  app: {
    title: 'app',
    description: 'description',
  },
  scalarDefinitions: [
    {
      name: 'test',
      regex: '^abc$',
    },
  ],
};

const input: InputDataInterface = {
  name: 'CreateUserInput',
  def: [
    {
      default: '"toto"',
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      def: [],
    },
    {
      default: '',
      name: 'password',
      type: 'Password!',
      description: undefined,
      def: [],
    },
  ],
  optionable: false,
};

const request: FilesDataInterface = {
  description: 'Create a user',
  errors: [],
  input: input,
  inputName: 'input',
  name: 'createUser',
  output: {
    name: 'UserInterface',
    def: [
      {
        name: 'name',
        type: 'UserName!',
        description: 'Username',
        def: [],
      },
    ],
    implementations: [],
    optionable: false,
    isArray: false,
  },
  requestType: 'query',
};

const requestString = '### Request\n```graphql\nquery {\n  createUser (\n    input: {\n      name: UserName\n      password: Password\n    }\n  ) {\n    name\n  }\n}\n```';
const responseString = '### Response\n```json\n{\n  "data": {\n    "createUser": {\n      "name": UserName\n    }\n  }\n}\n```';
const exampleString = `## Example\n${requestString}\n${responseString}`;

export { conf, request, exampleString };
