import { promises } from 'fs';

import { CreateTableInterface } from './createTable/createTable';
import { WriteExampleInterface } from './writeExample/writeExample';
import { FormatPathInterface } from '../../helpers/formatPath/formatPath';
import { ConfInterface } from '../../helpers/getJsonConfig/conf.interface';
import { GetApiRequestsInterface } from '../../helpers/getApiRequests/getApiRequests';
import { WriteTableOfContentInterface } from './writeTableOfContent/writeTableOfContent';
import { FilesDataInterface, RequestDataInterface } from '../../helpers/getApiRequests/requestData.interface';

export interface WriteDocInterface {
  execute(conf: ConfInterface, path: string): Promise<void>;
}

export class WriteDoc implements WriteDocInterface {
  private getRequests: GetApiRequestsInterface;
  private createTable: CreateTableInterface;
  private writeExample: WriteExampleInterface;
  private writeToc: WriteTableOfContentInterface;
  private formatPathName: FormatPathInterface;

  constructor(
    getRequests: GetApiRequestsInterface,
    createTable: CreateTableInterface,
    writeExample: WriteExampleInterface,
    writeToc: WriteTableOfContentInterface,
    formatPathName: FormatPathInterface,
  ) {
    this.getRequests = getRequests;
    this.createTable = createTable;
    this.writeExample = writeExample;
    this.writeToc = writeToc;
    this.formatPathName = formatPathName;
  }

  async execute(conf: ConfInterface, path: string): Promise<void> {
    const dataRequest: RequestDataInterface = await this.getRequests.execute(conf);

    // write a folder for each folder in dataRequest.folders and all containings files
    for (const folder of dataRequest.folders) {
      const folderPath = path + '/' + this.formatPathName.execute(folder.name);
      await promises.mkdir(folderPath);
      await this.write(folderPath, folder.files, conf);
    }
    // write a folder for dataRequest.rest and all containings files
    const pagesPath = path + '/pages';
    await promises.mkdir(pagesPath);
    await this.write(pagesPath, dataRequest.rest, conf);
  }

  private async write(path: string, requestArray: FilesDataInterface[], conf: ConfInterface) {
    const backLink = '../tableOfContent.md';
    for (const request of requestArray) {
      // add title
      let string = `[back](${backLink})\n# ${request.requestType + ' ' + request.name}\n \n`;

      // add description if is present
      if (request.description) string += `${request.description} \n \n`;

      if (request.input) string += '## Input\n' + (await this.createTable.execute(request.input, 'Input', path)) + '\n';

      if (request.output) string += '## Output\n' + (await this.createTable.execute(request.output, 'Output', path)) + '\n';

      // add errors list
      let errorsString = '## Errors\n';

      for (const error of request.errors) {
        const reason: string = error.reason ? error.reason : '';
        errorsString += `* \`${error.name}\` ${reason}\n`;
      }
      string += errorsString;

      // remove same line (mostly to remove same type table)
      let stringFiltered: string = Array.from(new Set(string.split(/\|\n\n/)).values()).join('\n');

      // write example for the request
      const exampleString = await this.writeExample.execute(request, conf);
      stringFiltered += exampleString;

      // add toc for the file
      const tocString: string = this.writeToc.execute(stringFiltered);
      const splitedString: string[] = stringFiltered.split(/\n/);

      // add the table of content
      splitedString.splice(1, 0, tocString);
      const fileContent: string = splitedString.join('\n');

      await promises.writeFile(path + `/${this.formatPathName.execute(request.name)}.md`, fileContent, 'utf8');
    }
  }
}
