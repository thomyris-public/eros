import { promises } from 'fs';
import { relative, posix, sep } from 'path';

import { FormatPathInterface } from '../../helpers/formatPath/formatPath';
import { GenerateFromTypeInterface } from '../../helpers/generateFromType/generateFromType';
import { ConfInterface, ScalarConfInterface } from '../../helpers/getJsonConfig/conf.interface';
import { DataElmt, SchemaInterface, SchemaProperty } from '../../getSchemaData/schema.interface';

export interface WriteAssetsInterface {
  execute(path: string, schema: SchemaInterface, conf: ConfInterface): Promise<void>;
}
export class WriteAssets implements WriteAssetsInterface {
  private generateFromType: GenerateFromTypeInterface;
  private formatPath: FormatPathInterface;

  constructor(generateFromType: GenerateFromTypeInterface, formatPath: FormatPathInterface) {
    this.generateFromType = generateFromType;
    this.formatPath = formatPath;
  }

  async execute(path: string, schema: SchemaInterface, conf: ConfInterface): Promise<void> {
    // create the assets folder
    const assetsPath: string = path + '/assets';
    await promises.mkdir(assetsPath);

    // loop on each schema's property
    for (const schemaProp in schema) {
      const element: SchemaProperty = schema[schemaProp];

      // create a folder for each properties in the schema
      const firstFolderPath: string = assetsPath + '/' + schemaProp + 's';
      await promises.mkdir(firstFolderPath);

      // loop on each record in the schema.property
      for (const dataElmt of element.data) {
        let subFolder: string = firstFolderPath;

        if (schemaProp === 'interface') {
          subFolder += `/${dataElmt.fileName}`;
          await promises.mkdir(subFolder);
        }

        // create file in the proper interface folder
        if (dataElmt.fileContent.includes('implements')) {
          const interfaceName: string = dataElmt.fileContent.split('implements ')[1].split(' ')[0];
          subFolder = subFolder.split('/').slice(0, -1).join('/') + `/interfaces/${interfaceName}`;
        }

        let formatedString: string = this.formatString(dataElmt);

        // check if the scalar is specified and add an example if it is
        if (schemaProp === 'scalar' && conf.scalarDefinitions) {
          const isSpecified: ScalarConfInterface[] = conf.scalarDefinitions.filter((scalar) => scalar.name === dataElmt.fileName);
          if (isSpecified[0]) {
            const example = await this.generateFromType.execute(dataElmt.fileName, conf);
            formatedString += `\n**Regex:** \`${isSpecified[0].regex}\`\n`;
            formatedString += `\n**Example:** \`${example}\``;
          }
        }
        const outputPath = `${conf.output.path}${conf.output.dirName}`;
        await this.writeAssetsFile(subFolder + '/', outputPath, formatedString, dataElmt.fileName);
      }
    }
  }

  private formatString(dataElmt: DataElmt): string {
    // remove one break line when there is two
    const fileContent = dataElmt.fileContent.replace(/\r\n\r\n/gm, '\n').replace(/(\n\n)|(\n\n\n)/, '\n');
    return `${dataElmt.comment}\n${'```graphql' + fileContent + '\n```'}\n`;
  }

  private async writeAssetsFile(path: string, rootDocPath: string, content: string, fileName: string) {
    let backPath = relative(path, rootDocPath);
    backPath = backPath.split(sep).join(posix.sep);

    const backLink = `[back](${backPath}/tableOfContent.md)\n\n`;
    content = backLink + content;
    await promises.writeFile(path + this.formatPath.execute(fileName) + '.md', content, 'utf8');
  }
}
