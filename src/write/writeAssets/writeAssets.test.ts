import { promises } from 'fs';

import { WriteAssets } from './writeAssets';
import { FormatPath } from '../../helpers/formatPath/formatPath';
import { GenFileHandler } from '../../helpers/genFileHandler/genFileHandler';
import { GenerateFromType } from '../../helpers/generateFromType/generateFromType';
import { conf, schemaImplements, schemaInput, schemaInterface, schemaScalar, schemaScalarSpecified } from './writeAssets.test.data';

describe('WriteAssets', () => {
  const path = './tests';

  const genFileHandler = new GenFileHandler();
  const generateFromType = new GenerateFromType(genFileHandler);
  const formatPath = new FormatPath();
  const writeAssets = new WriteAssets(generateFromType, formatPath);

  const spyMkdir = jest.spyOn(promises, 'mkdir');
  const spyWrite = jest.spyOn(promises, 'writeFile');
  const spyGenerate = jest.spyOn(generateFromType, 'execute');

  // add "as any" to spy private method
  const spyFormat = jest.spyOn(writeAssets as any, 'formatString');
  const spyWriteFile = jest.spyOn(writeAssets as any, 'writeAssetsFile');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should call mkdir, formatString and writeAssetsFile for an input', async () => {
    // arrange
    spyMkdir.mockResolvedValueOnce('/assets');
    spyMkdir.mockResolvedValueOnce('/assets/interfaces');
    spyMkdir.mockResolvedValueOnce('/assets/types');
    spyMkdir.mockResolvedValueOnce('/assets/inputs');
    spyWrite.mockResolvedValueOnce(); //assets/inputs/testfle.md
    spyMkdir.mockResolvedValueOnce('/assets/enums');
    spyMkdir.mockResolvedValueOnce('/assets/querys');
    spyMkdir.mockResolvedValueOnce('/assets/mutations');
    spyMkdir.mockResolvedValueOnce('/assets/subscriptions');
    spyMkdir.mockResolvedValueOnce('/assets/scalars');
    //act
    await writeAssets.execute(path, schemaInput, conf);
    //assert
    // folder "assets" + each schema property (8)
    expect(spyMkdir).toHaveBeenCalledTimes(9);
    expect(spyWrite).toHaveBeenCalledTimes(1);
    expect(spyFormat).toHaveBeenCalledTimes(1);
    expect(spyWriteFile).toHaveBeenCalledTimes(1);
  });

  it('Should call mkdir, formatString and writeAssetsFile for an interface', async () => {
    // arrange
    spyMkdir.mockResolvedValueOnce('/assets');
    spyMkdir.mockResolvedValueOnce('/assets/interfaces');
    spyMkdir.mockResolvedValueOnce('/assets/interfaces/testInterface');
    spyWrite.mockResolvedValueOnce(); //assets/interfaces/testInterface/testinterface.md
    spyMkdir.mockResolvedValueOnce('/assets/types');
    spyMkdir.mockResolvedValueOnce('/assets/inputs');
    spyMkdir.mockResolvedValueOnce('/assets/enums');
    spyMkdir.mockResolvedValueOnce('/assets/querys');
    spyMkdir.mockResolvedValueOnce('/assets/mutations');
    spyMkdir.mockResolvedValueOnce('/assets/subscriptions');
    spyMkdir.mockResolvedValueOnce('/assets/scalars');
    //act
    await writeAssets.execute(path, schemaInterface, conf);
    //assert
    // folder "assets" + each schema property (8) + one folder for the interface => 10
    expect(spyMkdir).toHaveBeenCalledTimes(10);
    expect(spyWrite).toHaveBeenCalledTimes(1);
    expect(spyFormat).toHaveBeenCalledTimes(1);
    expect(spyWriteFile).toHaveBeenCalledTimes(1);
  });

  it("Should call mkdir, formatString and writeAssetsFile for an implementation and it'interface", async () => {
    // arrange
    spyMkdir.mockResolvedValueOnce('/assets');
    spyMkdir.mockResolvedValueOnce('/assets/interfaces');
    spyMkdir.mockResolvedValueOnce('/assets/interfaces/testInterface');
    spyWrite.mockResolvedValueOnce(); //assets/interfaces/testInterface/testinterface.md
    spyMkdir.mockResolvedValueOnce('/assets/types');
    spyWrite.mockResolvedValueOnce(); //assets/interfaces/testInterface/testfile.md
    spyMkdir.mockResolvedValueOnce('/assets/inputs');
    spyMkdir.mockResolvedValueOnce('/assets/enums');
    spyMkdir.mockResolvedValueOnce('/assets/querys');
    spyMkdir.mockResolvedValueOnce('/assets/mutations');
    spyMkdir.mockResolvedValueOnce('/assets/subscriptions');
    spyMkdir.mockResolvedValueOnce('/assets/scalars');
    //act
    await writeAssets.execute(path, schemaImplements, conf);
    //assert

    // folder "assets" + each schema property (8) + one folder for the interface => 9
    expect(spyMkdir).toHaveBeenCalledTimes(10);
    expect(spyWrite).toHaveBeenCalledTimes(2);

    // implementation and it's interface => 2 call
    expect(spyFormat).toHaveBeenCalledTimes(2);
    expect(spyWriteFile).toHaveBeenCalledTimes(2);
  });

  it('Should call mkdir, formatString, generateFromType and writeAssetsFile for a specified scalar', async () => {
    // arrange
    spyMkdir.mockResolvedValueOnce('/assets');
    spyMkdir.mockResolvedValueOnce('/assets/interfaces');
    spyMkdir.mockResolvedValueOnce('/assets/types');
    spyMkdir.mockResolvedValueOnce('/assets/inputs');
    spyMkdir.mockResolvedValueOnce('/assets/enums');
    spyMkdir.mockResolvedValueOnce('/assets/querys');
    spyMkdir.mockResolvedValueOnce('/assets/mutations');
    spyMkdir.mockResolvedValueOnce('/assets/subscriptions');
    spyMkdir.mockResolvedValueOnce('/assets/scalars');
    spyGenerate.mockResolvedValueOnce(true);
    spyWrite.mockResolvedValueOnce(); //assets/scalars/customid.md
    //act
    await writeAssets.execute(path, schemaScalarSpecified, conf);
    //assert

    // folder "assets" + each schema property (8) => 9
    expect(spyMkdir).toHaveBeenCalledTimes(9);
    expect(spyWrite).toHaveBeenCalledTimes(1);

    // a scalar file => 1 call
    expect(spyFormat).toHaveBeenCalledTimes(1);
    expect(spyGenerate).toHaveBeenCalledTimes(1);
    expect(spyWriteFile).toHaveBeenCalledTimes(1);
  });

  it('Should call mkdir, formatString and writeAssetsFile but not generateFromType for a not specifed scalar', async () => {
    // arrange
    spyMkdir.mockResolvedValueOnce('/assets');
    spyMkdir.mockResolvedValueOnce('/assets/interfaces');
    spyMkdir.mockResolvedValueOnce('/assets/types');
    spyMkdir.mockResolvedValueOnce('/assets/inputs');
    spyMkdir.mockResolvedValueOnce('/assets/enums');
    spyMkdir.mockResolvedValueOnce('/assets/querys');
    spyMkdir.mockResolvedValueOnce('/assets/mutations');
    spyMkdir.mockResolvedValueOnce('/assets/subscriptions');
    spyMkdir.mockResolvedValueOnce('/assets/scalars');
    spyWrite.mockResolvedValueOnce(); //assets/scalars/not_spec.md
    //act
    await writeAssets.execute(path, schemaScalar, conf);
    //assert

    // folder "assets" + each schema property (8) => 9
    expect(spyMkdir).toHaveBeenCalledTimes(9);
    expect(spyWrite).toHaveBeenCalledTimes(1);

    // a scalar file => 1 call
    expect(spyFormat).toHaveBeenCalledTimes(1);
    expect(spyGenerate).toHaveBeenCalledTimes(0);
    expect(spyWriteFile).toHaveBeenCalledTimes(1);
  });
});
