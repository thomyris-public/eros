import { ConfInterface } from '../../helpers/getJsonConfig/conf.interface';
import { DataElmt, SchemaInterface } from '../../getSchemaData/schema.interface';

const conf: ConfInterface = {
  app: {
    title: 'title',
  },
  maxDepth: 2,
  input: '',
  output: {
    path: './tests',
    dirName: '',
  },
  scalarDefinitions: [
    {
      name: 'CustomID',
      regex: '^a$',
    },
  ],
};

const schema: SchemaInterface = {
  interface: {
    data: [],
  },
  type: {
    data: [],
  },
  input: {
    data: [],
  },
  enum: {
    data: [],
  },
  query: {
    data: [],
  },
  mutation: {
    data: [],
  },
  subscription: {
    data: [],
  },
  scalar: {
    data: [],
  },
};

const inputData: DataElmt = {
  fileContent: 'test input',
  comment: 'comment',
  fileName: 'testFile',
};

const interfaceData: DataElmt = {
  fileContent: 'testInterface',
  comment: 'comment',
  fileName: 'testInterface',
};

const implementData: DataElmt = {
  fileContent: 'test implements testInterface ',
  comment: 'comment',
  fileName: 'testFile',
};

const scalarSpecifiedData: DataElmt = {
  fileContent: 'scalar CustomID',
  comment: 'scalar for a CustomID',
  fileName: 'CustomID',
};

const scalarData: DataElmt = {
  fileContent: 'not spec',
  comment: '',
  fileName: 'not spec',
};

const schemaInput: SchemaInterface = { ...schema, input: { ...schema.input, data: [inputData] } };
const schemaInterface: SchemaInterface = { ...schema, interface: { ...schema.interface, data: [interfaceData] } };
const schemaImplements: SchemaInterface = { ...schemaInterface, type: { ...schemaInterface.type, data: [implementData] } };
const schemaScalarSpecified: SchemaInterface = { ...schema, scalar: { ...schema.input, data: [scalarSpecifiedData] } };
const schemaScalar: SchemaInterface = { ...schema, scalar: { ...schema.input, data: [scalarData] } };

export { conf, schemaInput, schemaInterface, schemaImplements, schemaScalarSpecified, schemaScalar };
