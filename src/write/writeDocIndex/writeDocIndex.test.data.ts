import { ConfInterface } from '../../helpers/getJsonConfig/conf.interface';
import { ScalarDataInterface } from '../../helpers/getScalars/ScalarDataInterface';
import { FilesDataInterface, RequestDataInterface } from '../../helpers/getApiRequests/requestData.interface';

const conf: ConfInterface = {
  app: {
    title: 'title',
  },
  maxDepth: 2,
  input: '',
  output: {
    path: './',
    dirName: '',
  },
};

const confWithDes: ConfInterface = {
  app: {
    title: 'title',
    description: 'description',
  },
  maxDepth: 2,
  input: '',
  output: {
    path: './',
    dirName: '',
  },
};

const emptyRequestData: RequestDataInterface = {
  folders: [],
  rest: [],
};

const filesData: FilesDataInterface = {
  name: 'CreateUser',
  description: 'create a user',
  errors: [],
  inputName: 'input',
  input: null,
  output: null,
  requestType: 'Query',
};

const folderRequestData: RequestDataInterface = {
  folders: [
    {
      files: [filesData],
      name: 'User',
    },
  ],
  rest: [],
};

const restRequestData: RequestDataInterface = {
  folders: [],
  rest: [filesData, filesData],
};

const scalarData: ScalarDataInterface = {
  name: 'test',
  link: 'link test',
};

const stringEmptyReqNoDes = '# title\n## Summary\n';
const stringEmptyReq = '# title\ndescription\n## Summary\n';
const stringFolderReq = stringEmptyReq + '### 1. User\n  - [CreateUser](./user/createuser.md)\n';
const stringRestReq = stringEmptyReq + '---\n### Others query\n- [CreateUser](./pages/createuser.md)\n- [CreateUser](./pages/createuser.md)\n';
const stringScalar = stringEmptyReq + `---\n### Scalars\n- [${scalarData.name}](${scalarData.link})\n- [${scalarData.name}](${scalarData.link})\n`;

export { conf, confWithDes, emptyRequestData, stringEmptyReqNoDes, stringEmptyReq, folderRequestData, stringFolderReq, restRequestData, stringRestReq, scalarData, stringScalar };
