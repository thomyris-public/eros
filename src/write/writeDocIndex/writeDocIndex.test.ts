import { promises } from 'fs';

import { WriteDocIndex } from './writeDocIndex';
import { GetScalars } from '../../helpers/getScalars/getScalars';
import { FormatPath } from '../../helpers/formatPath/formatPath';
import { GetApiRequests } from '../../helpers/getApiRequests/getApiRequests';
import {
  conf,
  confWithDes,
  emptyRequestData,
  folderRequestData,
  restRequestData,
  scalarData,
  stringEmptyReq,
  stringEmptyReqNoDes,
  stringFolderReq,
  stringRestReq,
  stringScalar,
} from './writeDocIndex.test.data';

describe('WriteDocIndex', () => {
  const getRequests = new GetApiRequests();
  const getScalars = new GetScalars();
  const formatPath = new FormatPath();
  const writeIndex = new WriteDocIndex(getRequests, getScalars, formatPath);

  const spyRequest = jest.spyOn(getRequests, 'execute');
  const spyScalar = jest.spyOn(getScalars, 'execute');
  const spyWrite = jest.spyOn(promises, 'writeFile');

  const path = './tests';

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should write the tableOfContent file by calling getRequests and writeFile', async () => {
    // arrange
    const fileContent = `# title
## Summary
`;
    spyRequest.mockResolvedValueOnce(emptyRequestData);
    spyScalar.mockResolvedValueOnce([]);
    spyWrite.mockResolvedValueOnce(); // write '/tableOfContent.md'
    // act
    await writeIndex.execute(conf, path);
    // assert
    expect(spyRequest).toHaveBeenCalledTimes(1);
    expect(spyScalar).toHaveBeenCalledTimes(1);
    expect(spyWrite).toHaveBeenCalledTimes(1);
    expect(fileContent).toStrictEqual(stringEmptyReqNoDes);
  });

  it('Should write the tableOfContent with a description', async () => {
    // arrange
    const fileContent = `# title
description
## Summary
`;
    spyRequest.mockResolvedValueOnce(emptyRequestData);
    spyScalar.mockResolvedValueOnce([]);
    spyWrite.mockResolvedValueOnce(); // write '/tableOfContent.md'
    // act
    await writeIndex.execute(confWithDes, path);
    // assert
    expect(spyRequest).toHaveBeenCalledTimes(1);
    expect(spyScalar).toHaveBeenCalledTimes(1);
    expect(spyWrite).toHaveBeenCalledTimes(1);
    expect(fileContent).toStrictEqual(stringEmptyReq);
  });

  it('Should write the tableOfContent with a request in a folder', async () => {
    // arrange
    const fileContent = `# title
description
## Summary
### 1. User
  - [CreateUser](./user/createuser.md)
`;
    spyRequest.mockResolvedValueOnce(folderRequestData);
    spyScalar.mockResolvedValueOnce([]);
    spyWrite.mockResolvedValueOnce(); // write '/tableOfContent.md'
    // act
    await writeIndex.execute(confWithDes, path);
    // assert
    expect(spyRequest).toHaveBeenCalledTimes(1);
    expect(spyScalar).toHaveBeenCalledTimes(1);
    expect(spyWrite).toHaveBeenCalledTimes(1);
    expect(fileContent).toStrictEqual(stringFolderReq);
  });

  it('Should write the tableOfContent with a query and a mutation in the rest', async () => {
    // arrange
    const fileContent = `# title
description
## Summary
---
### Others query
- [CreateUser](./pages/createuser.md)
- [CreateUser](./pages/createuser.md)
`;
    spyRequest.mockResolvedValueOnce(restRequestData);
    spyScalar.mockResolvedValueOnce([]);
    spyWrite.mockResolvedValueOnce(); // write '/tableOfContent.md'
    // act
    await writeIndex.execute(confWithDes, path);
    // assert
    expect(spyRequest).toHaveBeenCalledTimes(1);
    expect(spyScalar).toHaveBeenCalledTimes(1);
    expect(spyWrite).toHaveBeenCalledTimes(1);
    expect(fileContent).toStrictEqual(stringRestReq);
  });

  it('Should write the tableOfContent with a scalar', async () => {
    // arrange
    const fileContent = `# title
description
## Summary
---
### Scalars
- [test](link test)
- [test](link test)
`;
    spyRequest.mockResolvedValueOnce(emptyRequestData);
    spyScalar.mockResolvedValueOnce([scalarData, scalarData]);
    spyWrite.mockResolvedValueOnce(); // write '/tableOfContent.md'
    // act
    await writeIndex.execute(confWithDes, path);
    // assert
    expect(spyRequest).toHaveBeenCalledTimes(1);
    expect(spyScalar).toHaveBeenCalledTimes(1);
    expect(spyWrite).toHaveBeenCalledTimes(1);
    expect(fileContent).toStrictEqual(stringScalar);
  });
});
