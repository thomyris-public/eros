import { promises } from 'fs';

import { GetScalarsInterface } from '../../helpers/getScalars/getScalars';
import { FormatPathInterface } from '../../helpers/formatPath/formatPath';
import { ConfInterface } from '../../helpers/getJsonConfig/conf.interface';
import { ScalarDataInterface } from '../../helpers/getScalars/ScalarDataInterface';
import { GetApiRequestsInterface } from '../../helpers/getApiRequests/getApiRequests';
import { FolderDataInterface, RequestDataInterface } from '../../helpers/getApiRequests/requestData.interface';

export interface WriteDocIndexInterface {
  execute(conf: ConfInterface, path: string): Promise<void>;
}

export class WriteDocIndex implements WriteDocIndexInterface {
  private getRequests: GetApiRequestsInterface;
  private getScalars: GetScalarsInterface;
  private formatPath: FormatPathInterface;

  constructor(getRequests: GetApiRequestsInterface, getScalars: GetScalarsInterface, formatPath: FormatPathInterface) {
    this.getRequests = getRequests;
    this.getScalars = getScalars;
    this.formatPath = formatPath;
  }

  async execute(conf: ConfInterface, path: string): Promise<void> {
    const dataRequest: RequestDataInterface = await this.getRequests.execute(conf);
    const dataScalar: ScalarDataInterface[] = await this.getScalars.execute(conf);

    // add title
    let string = `# ${conf.app.title}\n`;
    // add description if is present
    if (conf.app.description) {
      string += `${conf.app.description}\n`;
    }

    string += '## Summary\n';

    for (let i = 0; i < dataRequest.folders.length; i++) {
      const folder: FolderDataInterface = dataRequest.folders[i];
      string += `### ${i + 1}. ${folder.name}\n`;

      for (const request of folder.files) {
        const formatedFolderName = this.formatPath.execute(folder.name);

        const formatedRequestName = this.formatPath.execute(request.name);

        // add link to the related page
        string += `  - [${request.name}](./${formatedFolderName}/${formatedRequestName}.md)\n`;
      }
    }

    let requestType = '';

    for (const request of dataRequest.rest) {
      // write the line only when we change type , the request are sorted by requestType so it will only write 2 times
      if (request.requestType !== requestType) {
        string += `---\n### Others ${request.requestType.toLowerCase()}\n`;
        requestType = request.requestType;
      }

      const formatedRequestName = this.formatPath.execute(request.name);

      // add link to the related page
      string += `- [${request.name}](./pages/${formatedRequestName}.md)\n`;
    }

    for (let i = 0; i < dataScalar.length; i++) {
      if (i < 1) string += '---\n### Scalars\n';
      const scalarFile = dataScalar[i];
      string += `- [${this.formatPath.execute(scalarFile.name)}](${scalarFile.link})\n`;
    }

    await promises.writeFile(path + '/tableOfContent.md', string, 'utf8');
  }
}
