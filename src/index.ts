#!/usr/bin/env node

import { Eros } from './eros';
import { Write } from './write/write';
import { Logger } from './helpers/logger/logger';
import { WriteDoc } from './write/writeDoc/writeDoc';
import { FormatPath } from './helpers/formatPath/formatPath';
import { GetScalars } from './helpers/getScalars/getScalars';
import { GetSchemaData } from './getSchemaData/getSchemaData';
import { WriteAssets } from './write/writeAssets/writeAssets';
import { SplitGqlSchema } from './splitGqlSchema/splitGqlSchema';
import { ErrorHandler } from './helpers/errorHandler/errorHandler';
import { WriteDocIndex } from './write/writeDocIndex/writeDocIndex';
import { GetJsonConfig } from './helpers/getJsonConfig/getJsonConfig';
import { CreateTable } from './write/writeDoc/createTable/createTable';
import { GetApiRequests } from './helpers/getApiRequests/getApiRequests';
import { GenFileHandler } from './helpers/genFileHandler/genFileHandler';
import { GetIndentation } from './helpers/getIndentation/getIndentation';
import { WriteExample } from './write/writeDoc/writeExample/writeExample';
import { GenerateFromType } from './helpers/generateFromType/generateFromType';
import { CreateLinkToAssets } from './helpers/createLinkToAssets/createLinkToAssets';
import { WriteTableOfContent } from './write/writeDoc/writeTableOfContent/writeTableOfContent';
import { WriteRequestExample } from './write/writeDoc/writeExample/writeRequestExample/writeRequestExample';
import { WriteResponseExample } from './write/writeDoc/writeExample/writeResponseExample/writeResponseExample';

// helpers
const getJsonConfig = new GetJsonConfig();
const getIndentation = new GetIndentation();
const handleGenFile = new GenFileHandler();
const generateFromType = new GenerateFromType(handleGenFile);
const getRequests = new GetApiRequests();
const getScalars = new GetScalars();
const logger = new Logger();
const errorHandler = new ErrorHandler(logger);
const formatPath = new FormatPath();
const createLink = new CreateLinkToAssets(getJsonConfig, formatPath);

// splitSchema class => split the gql schema
const splitSchema = new SplitGqlSchema();

// getSchema class => create the "schema", objects with all information from the splited gql schema
const getSchema = new GetSchemaData();

// write class => write all files
// writeAssets class => write assets/files
const writeAssets = new WriteAssets(generateFromType, formatPath);

// WriteDoc class => write doc/files
const createTable = new CreateTable(createLink);
const writeRequest = new WriteRequestExample(getIndentation, generateFromType);
const writeResponse = new WriteResponseExample(getIndentation, generateFromType);
const writeExample = new WriteExample(writeRequest, writeResponse);
const writeToc = new WriteTableOfContent();
const writeDoc = new WriteDoc(getRequests, createTable, writeExample, writeToc, formatPath);

// writeIndex class => write tableOfContent.md file
const writeIndex = new WriteDocIndex(getRequests, getScalars, formatPath);

const write = new Write(writeAssets, writeDoc, writeIndex);

// run the script
new Eros(getJsonConfig, splitSchema, getSchema, write, errorHandler, logger).execute();
