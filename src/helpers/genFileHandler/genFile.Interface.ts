export interface GenFileInterface {
  datas: GenFileDatasInterface[];
}

export interface GenFileDatasInterface {
  type: string;
  value: string | number | boolean;
  regex?: string;
}
