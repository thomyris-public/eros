import { existsSync, promises } from 'fs';

import { GenFileDatasInterface, GenFileInterface } from './genFile.Interface';
import { ObjectTypeInterface } from '../generateFromType/objectType.interface';

export interface GenFileHandlerInterface {
  write(objectType: ObjectTypeInterface, value: string | number | boolean): Promise<void>;
  getType(type: string): Promise<GenFileDatasInterface | undefined>;
}

export class GenFileHandler implements GenFileHandlerInterface {
  private path = './eros.data.json';

  private async read(): Promise<GenFileInterface> {
    let json: GenFileInterface = { datas: [] };

    if (existsSync(this.path)) {
      const file = await promises.readFile(this.path, 'utf8');
      json = JSON.parse(file);
    }

    return json;
  }

  async write(objectType: ObjectTypeInterface, value: string | number | boolean): Promise<void> {
    const json = await this.read();
    const datas: GenFileDatasInterface[] = json.datas;
    // is the type already in the file
    const typeIndex = datas.findIndex((elmt) => elmt.type === objectType.type);

    const newType: GenFileDatasInterface = {
      type: objectType.type,
      value: value,
      regex: objectType.regex ? objectType.regex : undefined,
    };

    // if object already present update it
    if (typeIndex !== -1) {
      datas[typeIndex] = newType;
    } else {
      datas.push(newType);
    }

    const jsonString = JSON.stringify(json, null, 2);
    await promises.writeFile(this.path, jsonString, 'utf8');
  }

  async getType(type: string): Promise<GenFileDatasInterface | undefined> {
    const json: GenFileInterface = await this.read();
    const objectType: GenFileDatasInterface | undefined = json.datas!.find((elmt) => elmt.type === type);
    return objectType;
  }
}
