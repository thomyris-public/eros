import * as fs from 'fs';

import { GenFileHandler } from './genFileHandler';
import { ObjectTypeInterface } from '../generateFromType/objectType.interface';

describe('GenFileHandler', () => {
  const genFileHandler = new GenFileHandler();

  const spyRead = jest.spyOn(fs.promises, 'readFile');
  const spyWrite = jest.spyOn(fs.promises, 'writeFile');
  const spyExist = jest.spyOn(fs, 'existsSync');

  describe('#write', () => {
    it('Should create the genFile', async () => {
      // arrange
      const fileString = `{
  "datas": [
    {
      "type": "test",
      "value": "test"
    }
  ]
}`;
      const objectType: ObjectTypeInterface = {
        type: 'test',
      };

      spyExist.mockReturnValueOnce(false);
      spyWrite.mockResolvedValueOnce();
      // act
      await genFileHandler.write(objectType, 'test');
      // assert
      expect(spyWrite).toHaveBeenNthCalledWith(1, './eros.data.json', fileString, 'utf8');
    });

    it('Should update a type in the genFile', async () => {
      // arrange
      const fileString = `{
  "datas": [
    {
      "type": "test",
      "value": "test1"
    }
  ]
}`;

      const objectType: ObjectTypeInterface = {
        type: 'test',
      };

      spyExist.mockReturnValueOnce(true); // genFile already exist
      spyRead.mockResolvedValueOnce('{"datas": [{"type": "test", "value": "test"}]}');
      spyWrite.mockResolvedValueOnce();
      // act
      await genFileHandler.write(objectType, 'test1');
      // assert
      expect(spyWrite).toHaveBeenNthCalledWith(2, './eros.data.json', fileString, 'utf8');
    });

    it('Should update a type with a regex in the genFile', async () => {
      // arrange
      const fileString = `{
  "datas": [
    {
      "type": "test",
      "value": "test1",
      "regex": "/^abc$/"
    }
  ]
}`;

      const objectType: ObjectTypeInterface = {
        type: 'test',
        regex: '/^abc$/',
      };

      spyExist.mockReturnValueOnce(true); // genFile already exist
      spyRead.mockResolvedValueOnce('{"datas": [{"type": "test", "value": "test"}]}');
      spyWrite.mockResolvedValueOnce();
      // act
      await genFileHandler.write(objectType, 'test1');
      // assert
      expect(spyWrite).toHaveBeenNthCalledWith(3, './eros.data.json', fileString, 'utf8');
    });
  });

  describe('#getType', () => {
    it('Should return the value for this type', async () => {
      // arrange
      spyExist.mockReturnValueOnce(true); // genFile already exist
      spyRead.mockResolvedValueOnce('{"datas": [{"type": "test", "value": "test"}]}');
      // act
      const result = await genFileHandler.getType('test');
      // assert
      expect(result).toBeTruthy();
      expect(result).toStrictEqual({ type: 'test', value: 'test' });
    });
  });
});
