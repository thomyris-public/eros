export const gqlBuiltInType: gqlBuiltInTypeInterface = {
  Float: 'Float',
  String: 'String',
  Boolean: 'Boolean',
  Int: 'Int',
  ID: 'ID',
};

interface gqlBuiltInTypeInterface {
  [key: string]: any;
}
