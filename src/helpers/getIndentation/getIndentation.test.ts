import { GetIndentation } from './getIndentation';

describe('GetIndentation', () => {
  const getIndentation = new GetIndentation();

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should return a string with the specified number of tabs here 2', () => {
    // arrange
    // act
    const result = getIndentation.execute(2);
    //assert
    expect(result).toStrictEqual('    ');
  });
});
