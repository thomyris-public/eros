export interface GetIndentationInterface {
  execute(nbr: number): string;
}

export class GetIndentation implements GetIndentationInterface {
  execute(nbr: number): string {
    return '  '.repeat(nbr);
  }
}
