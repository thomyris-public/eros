import { join, posix, sep } from 'path';
import { promises } from 'fs';

import { ScalarDataInterface } from './ScalarDataInterface';
import { ConfInterface } from '../getJsonConfig/conf.interface';

export interface GetScalarsInterface {
  execute(conf: ConfInterface): Promise<ScalarDataInterface[]>;
}

export class GetScalars implements GetScalarsInterface {
  async execute(conf: ConfInterface): Promise<ScalarDataInterface[]> {
    const path = join(conf.output.path, conf.output.dirName) + '/assets/scalars';

    const scalarFolder = await promises.readdir(path);
    const scalarData: ScalarDataInterface[] = [];

    for (let i = 0; i < scalarFolder.length; i++) {
      const scalarFile = scalarFolder[i];

      if (conf.scalarDefinitions) {
        const isSpecified = conf.scalarDefinitions.filter((scalarDefinition) => scalarDefinition.name === scalarFile.replace('.md', ''));

        // add the scalar in the data returned if it is specified
        if (isSpecified[0]) {
          const assetsPatch = join('./assets/scalars', scalarFile);
          const scalarObject: ScalarDataInterface = {
            name: scalarFile.replace('.md', ''),
            link: assetsPatch.split(sep).join(posix.sep),
          };
          scalarData.push(scalarObject);
        }
      }
    }

    return scalarData;
  }
}
