import { ConfInterface } from '../getJsonConfig/conf.interface';

const conf: ConfInterface = {
  app: {
    title: 'title',
  },
  maxDepth: 2,
  input: '',
  output: {
    path: './',
    dirName: '',
  },
};

const confWithScalar: ConfInterface = {
  app: {
    title: 'title',
  },
  maxDepth: 2,
  input: '',
  output: {
    path: './',
    dirName: '',
  },
  scalarDefinitions: [
    {
      name: 'test',
      regex: '^a$',
    },
  ],
};

export { conf, confWithScalar };
