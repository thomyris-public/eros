import { promises } from 'fs';

import { GetScalars } from './getScalars';
import { conf, confWithScalar } from './getScalars.test.data';

describe('GetScalars', () => {
  const getScalars = new GetScalars();

  const spyReaddir = jest.spyOn(promises, 'readdir');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should return nothing', async () => {
    // arrange
    spyReaddir.mockResolvedValueOnce(['test' as any]);
    // act
    const result = await getScalars.execute(conf);
    // assert
    expect(spyReaddir).toHaveBeenCalledTimes(1);
    expect(result).toStrictEqual([]);
  });

  it('Should return nothing', async () => {
    // arrange
    spyReaddir.mockResolvedValueOnce(['no match' as any]);
    // act
    const result = await getScalars.execute(confWithScalar);
    // assert
    expect(spyReaddir).toHaveBeenCalledTimes(1);
    expect(result).toStrictEqual([]);
  });

  it('Should return the specified scalars', async () => {
    // arrange
    spyReaddir.mockResolvedValueOnce(['test' as any]);
    // act
    await getScalars.execute(confWithScalar);
    // assert
    expect(spyReaddir).toHaveBeenCalledTimes(1);
  });
});
