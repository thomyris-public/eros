import { colours } from './logColours';
import { version } from '../../../package.json';
import { TimeConverter } from '../timeConverter/timeConverter';

export interface LoggerInterface {
  error(message: string): void;
  info(message: string): void;
  time(message: string, ms: number): void;
}

export class Logger implements LoggerInterface {
  private baseColour = colours.reset;
  private headingColour = colours.fg.magenta;
  private errorColour = colours.fg.red;
  private infoColour = colours.fg.green;

  private heading = `${this.headingColour}Eros_gql version ${version}${this.baseColour}\n`;

  error(message: string) {
    const errorString = `${this.heading}${this.errorColour}Error: ${message}${this.baseColour}`;
    console.error(errorString);
  }

  info(message: string) {
    const infoString = `${this.heading}${this.infoColour}Info: ${message}${this.baseColour}`;
    console.info(infoString);
  }

  time(message: string, ms: number) {
    const duration = TimeConverter.convert(ms);

    this.info(`${message} ${duration}`);
  }
}
