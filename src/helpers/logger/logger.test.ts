import { Logger } from './logger';
import { colours } from './logColours';

describe('Logger', () => {
  const logger = new Logger();
  const spyError = jest.spyOn(console, 'error');
  const spyInfo = jest.spyOn(console, 'info');
  /* eslint @typescript-eslint/no-empty-function: "off" */
  beforeAll(() => {
    // avoid geting the log message in the terminal comment to see log in console
    spyError.mockImplementation(() => {});
    spyInfo.mockImplementation(() => {});
  });

  const heading = `${colours.fg.magenta}Eros_gql version ${process.env.npm_package_version}${colours.reset}\n`;
  it('should call console.error with the given message', () => {
    // arrange
    const message = 'test';
    const errorString = `${heading}${colours.fg.red}Error: ${message}${colours.reset}`;
    // act
    logger.error(message);
    // assert
    expect(spyError).toHaveBeenNthCalledWith(1, errorString);
  });

  it('should call console.info with the given message', () => {
    // arrange
    const message = 'test';
    const infoString = `${heading}${colours.fg.green}Info: ${message}${colours.reset}`;
    // act
    logger.info(message);
    // assert
    expect(spyInfo).toHaveBeenNthCalledWith(1, infoString);
  });
});
