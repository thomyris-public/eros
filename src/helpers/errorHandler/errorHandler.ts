import { LoggerInterface } from '../logger/logger';

export interface ErrorHandlerInterface {
  execute(error: any): void;
}

export class ErrorHandler implements ErrorHandlerInterface {
  private logger: LoggerInterface;

  constructor(logger: LoggerInterface) {
    this.logger = logger;
  }

  execute(error: any) {
    if (error && error.stack && error.message) {
      // should be an error
      this.logger.error(error.message);
      return;
    }
    this.logger.error(error);
  }
}
