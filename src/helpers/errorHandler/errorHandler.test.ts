import { Logger } from '../logger/logger';
import { ErrorHandler } from './errorHandler';

describe('ErrorHandler', () => {
  const logger = new Logger();
  const errorHandler = new ErrorHandler(logger);
  const spyLogError = jest.spyOn(logger, 'error');

  const spyError = jest.spyOn(console, 'error');
  const spyInfo = jest.spyOn(console, 'info');
  /* eslint @typescript-eslint/no-empty-function: "off" */
  beforeAll(() => {
    // avoid geting the log message in the terminal comment to see log in console
    spyError.mockImplementation(() => {});
    spyInfo.mockImplementation(() => {});
  });

  it('Should call logger.error if it get an error object', () => {
    // arrange
    const error = new Error('test');
    // act
    errorHandler.execute(error);
    // assert
    expect(spyLogError).toHaveBeenNthCalledWith(1, error.message);
  });

  it('Should call logger.error if it get smth else', () => {
    // arrange
    const error = ['test'];
    // act
    errorHandler.execute(error);
    // assert
    expect(spyLogError).toHaveBeenNthCalledWith(2, error);
  });
});
