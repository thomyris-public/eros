import { FilesDataInterface, InputDataInterface, OutputDataInterface } from './requestData.interface';
import { ConfInterface, ErrorConfInterface, FileConfInterface } from '../getJsonConfig/conf.interface';

const conf: ConfInterface = {
  app: {
    title: 'title',
  },
  maxDepth: 2,
  input: '',
  output: {
    path: './',
    dirName: '',
  },
};

const confTree: ConfInterface = {
  app: {
    title: 'title',
  },
  maxDepth: 2,
  input: '',
  output: {
    path: './',
    dirName: '',
  },
  tree: {
    test: [
      {
        requestName: 'getUsers',
      },
    ],
  },
};

const errorConf: FileConfInterface = {
  requestName: 'getUsers',
  errors: [
    {
      name: 'test error',
      reason: 'reason error',
    },
  ],
};

const confTreeError: ConfInterface = { ...confTree, tree: { ...confTree.tree, test: [errorConf] } };

const queryString = '[back](../../tableOfContent.md)\r\n\r\n```graphql\r\ntype Query {\r\n  """Get users"""\r\n  getUsers: [UserType!]!\r\n}\r\n```';

const queryObject: FilesDataInterface = {
  description: 'Get users',
  errors: [],
  input: null,
  inputName: null,
  name: 'getUsers',
  output: '[UserType!]!',
  requestType: 'query',
};

const subscriptionObject: FilesDataInterface = { ...queryObject, requestType: 'subscription' };

const queryStringLgth = '[back](../../tableOfContent.md)\r\n\r\n```graphql\r\ntype Query {\r\n  """Get users"""\r\n  testLgth: [test!]!\r\n}\r\n```';

const queryStringSort =
  '[back](../../tableOfContent.md)\r\n\r\n```graphql\r\ntype Query {\r\n  """Get users"""\r\n  getUsers: [UserType!]!\r\n"""Get users"""\r\n  test: [UserType!]!\r\n}\r\n```';

const queryObject2: FilesDataInterface = {
  description: 'Get users',
  errors: [],
  input: null,
  inputName: null,
  name: 'test',
  output: '[UserType!]!',
  requestType: 'query',
};

const queryObjectError: FilesDataInterface = {
  description: 'Get users',
  errors: [errorConf.errors![0] as ErrorConfInterface],
  input: null,
  inputName: null,
  name: 'getUsers',
  output: '[UserType!]!',
  requestType: 'query',
};

const mutationString =
  '[back](../../tableOfContent.md)\r\n\r\n```graphql\r\ntype Mutation {\r\n  """Create a user"""\r\n  createUser(input: CreateUserInput!): UserType\r\n}\r\n```';

const mutationObject: FilesDataInterface = {
  description: 'Create a user',
  errors: [],
  input: null,
  inputName: 'input',
  name: 'createUser',
  output: 'UserType',
  requestType: 'mutation',
};

const inputString =
  '[back](../../tableOfContent.md)\r\n```graphql\r\ninput CreateUserInput {\r\n  """Username"""\r\n name: UserName! = "toto"\r\n  password: Password!\r\n}\r\n```';

const inputStringWithDepr =
  '[back](../../tableOfContent.md)\r\n```graphql\r\ninput CreateUserInput {\r\n  """Username"""\r\n name: UserName! = "toto"\r\n  password: Password! @deprecated(reason: "nope")\r\n}\r\n```';

const input: InputDataInterface = {
  name: 'CreateUserInput',
  def: [
    {
      default: '"toto"',
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      deprecated: undefined,
      def: [],
    },
    {
      default: '',
      name: 'password',
      type: 'Password!',
      description: undefined,
      deprecated: undefined,
      def: [],
    },
  ],
  optionable: false,
};

const inputWithDepr: InputDataInterface = {
  name: 'CreateUserInput',
  def: [
    {
      default: '"toto"',
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      deprecated: undefined,
      def: [],
    },
    {
      default: '',
      name: 'password',
      type: 'Password!',
      description: undefined,
      deprecated: 'nope',
      def: [],
    },
  ],
  optionable: false,
};

const inputSubInput: InputDataInterface = {
  ...input,
  def: [
    {
      ...input.def[0],
      def: [
        {
          name: 'UserName',
          optionable: false,
          def: input.def,
        },
      ],
    },
    input.def[1],
  ],
};

const mutationObjectInput: FilesDataInterface = { ...mutationObject, input: input };
const mutationObjectInputWithDepr: FilesDataInterface = { ...mutationObject, input: inputWithDepr };
const mutationObjectSubInput: FilesDataInterface = { ...mutationObjectInput, input: inputSubInput };

const mutationOutputString =
  '[back](../../tableOfContent.md)\r\n\r\n```graphql\r\ntype Mutation {\r\n  """Create a user"""\r\n  createUser(input: CreateUserInput!): UserInterface!\r\n}\r\n```';

const mutationNoOutput = '[back](../../tableOfContent.md)\r\n\r\n```graphql\r\ntype Mutation {\r\n  createUser(input: CreateUserInput! = "toto")\r\n}\r\n```';

const outputString = '[back](../../tableOfContent.md)\r\n```graphql\r\ninterface UserInterface {\r\n  """Username"""\r\n name: UserName!\r\n}\r\n```';

const outputStringDirect = '[back](../../tableOfContent.md)\r\n```graphql\r\ninterface UserInterface {\r\n  """Username"""\r\n name: UserName! @directive\r\n}\r\n```';

const implString = '[back](../../tableOfContent.md)\r\n```graphql\r\ninterface UserInterface {\r\n  """Username"""\r\n name: Username!\r\n test: Test!\r\n length: Int!}\r\n```';

const implStringWithDepr =
  '[back](../../tableOfContent.md)\r\n```graphql\r\ninterface UserInterface {\r\n  """Username"""\r\n name: Username!\r\n test: Test! @deprecated(reason: "nope")\r\n length: Int!}\r\n```';

const output: OutputDataInterface = {
  name: 'UserInterface',
  def: [
    {
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      deprecated: undefined,
      def: [],
    },
  ],
  implementations: [],
  optionable: false,
  isArray: false,
};

const outputEmptyDef: OutputDataInterface = {
  name: 'UserInterface',
  def: [
    {
      name: 'name',
      type: 'UserName!',
      description: 'Username',
      deprecated: undefined,
      def: [],
    },
  ],
  implementations: [],
  optionable: false,
  isArray: false,
};

const outputSubOutput: OutputDataInterface = { ...output, def: [...output.def] };
outputSubOutput.def[0].def = { ...output.def[0].def };

outputSubOutput.def[0].def = [
  {
    name: 'UserName',
    optionable: false,
    def: [],
    implementations: [],
    isArray: false,
  },
];

const outputInterface: OutputDataInterface = {
  ...outputEmptyDef,
  implementations: [
    {
      name: 'UserInterface',
      def: [
        {
          name: 'test',
          description: undefined,
          type: 'Test!',
          deprecated: undefined,
          def: [],
        },
        {
          name: 'lgth',
          type: 'Int!',
          description: undefined,
          deprecated: undefined,
          def: [],
        },
      ],
    },
  ],
};

const outputInterfaceWithDepr: OutputDataInterface = {
  ...outputEmptyDef,
  implementations: [
    {
      name: 'UserInterface',
      def: [
        {
          name: 'test',
          description: undefined,
          type: 'Test!',
          deprecated: 'nope',
          def: [],
        },
        {
          name: 'lgth',
          type: 'Int!',
          description: undefined,
          deprecated: undefined,
          def: [],
        },
      ],
    },
  ],
};

const mutationObjectOutput: FilesDataInterface = { ...mutationObject, output: outputEmptyDef };

const mutationObjectOutputSub: FilesDataInterface = { ...mutationObject, output: outputSubOutput };
const mutationObjectNotFound: FilesDataInterface = { ...mutationObject, output: 'UserInterface!' };

const mutationObjectOutputInterface: FilesDataInterface = { ...mutationObject, output: outputInterface };

const mutationObjectOutputInterfaceWithDepr: FilesDataInterface = { ...mutationObject, output: outputInterfaceWithDepr };

const mutationObjectNoOutput: FilesDataInterface = { ...mutationObject, description: null, output: null };

export {
  conf,
  confTree,
  queryString,
  confTreeError,
  queryObject,
  queryObjectError,
  mutationString,
  mutationObject,
  queryStringSort,
  queryObject2,
  inputString,
  mutationObjectInput,
  mutationObjectSubInput,
  mutationOutputString,
  implString,
  outputString,
  mutationObjectOutputInterface,
  mutationObjectOutputSub,
  mutationObjectOutput,
  mutationNoOutput,
  mutationObjectNoOutput,
  mutationObjectNotFound,
  queryStringLgth,
  subscriptionObject,
  outputStringDirect,
  inputStringWithDepr,
  mutationObjectInputWithDepr,
  implStringWithDepr,
  mutationObjectOutputInterfaceWithDepr,
};
