import { ErrorConfInterface } from '../getJsonConfig/conf.interface';

export interface RequestDataInterface {
  folders: FolderDataInterface[];
  rest: FilesDataInterface[];
}

export interface FolderDataInterface {
  name: string;
  files: FilesDataInterface[];
}

export interface FilesDataInterface {
  name: string;
  input: InputDataInterface | null;
  inputName: string | null;
  output: OutputDataInterface | string | null;
  description: string | null;
  requestType: string;
  errors: ErrorConfInterface[];
}

export interface InputDataInterface {
  name: string;
  optionable: boolean;
  def: InputDefInterface[];
}

export interface OutputDataInterface {
  name: string;
  optionable: boolean;
  isArray: boolean;
  def: OutputDefInterface[];
  implementations: ImplementationsDataInterface[];
}

export interface ImplementationsDataInterface {
  name: string;
  def: OutputDefInterface[];
}

export interface InputDefInterface {
  name: string;
  type: string;
  default: string;
  def: InputDataInterface[];
  description?: string;
  deprecated?: string;
}

export interface OutputDefInterface {
  name: string;
  type: string;
  def: Array<OutputDataInterface | string>;
  description?: string;
  deprecated?: string;
}
