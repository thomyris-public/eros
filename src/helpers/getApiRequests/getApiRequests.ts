import { join } from 'path';
import { existsSync, promises } from 'fs';

import { ConfInterface } from '../getJsonConfig/conf.interface';
import {
  FilesDataInterface,
  FolderDataInterface,
  ImplementationsDataInterface,
  InputDataInterface,
  InputDefInterface,
  OutputDataInterface,
  OutputDefInterface,
  RequestDataInterface,
} from './requestData.interface';

export interface GetApiRequestsInterface {
  execute(conf: ConfInterface): Promise<RequestDataInterface>;
}

export class GetApiRequests implements GetApiRequestsInterface {
  private maxDepth = 0;

  async execute(conf: ConfInterface): Promise<RequestDataInterface> {
    let queryFile: string | undefined;
    let mutationFile: string | undefined;
    let subscriptionFile: string | undefined;

    this.maxDepth = conf.maxDepth;

    const queryPath = join(conf.output.path, conf.output.dirName) + '/assets/querys/query.md';
    const mutationPath = join(conf.output.path, conf.output.dirName) + '/assets/mutations/mutation.md';
    const subscriptionPath = join(conf.output.path, conf.output.dirName) + '/assets/subscriptions/subscription.md';

    // read the request file to use it
    if (existsSync(queryPath)) queryFile = await promises.readFile(queryPath, 'utf8');
    if (existsSync(mutationPath)) mutationFile = await promises.readFile(mutationPath, 'utf8');
    if (existsSync(subscriptionPath)) subscriptionFile = await promises.readFile(subscriptionPath, 'utf8');

    const queryArray = queryFile ? await this.formatRequestData(queryFile, 'query', conf) : [];
    const mutationArray = mutationFile ? await this.formatRequestData(mutationFile, 'mutation', conf) : [];
    const subscriptionArray = subscriptionFile ? await this.formatRequestData(subscriptionFile, 'subscription', conf) : [];

    const requestArray = queryArray.concat(mutationArray).concat(subscriptionArray);

    // get requests specified in json config
    const requestsData: RequestDataInterface = {
      folders: [],
      rest: [],
    };

    // loop on each folder specified in genDoc.json config file
    for (const folder in conf.tree) {
      const folderData: FolderDataInterface = {
        name: folder,
        files: [],
      };

      // loop on each file of a folder in the genDoc.json config
      for (const file of conf.tree[folder]) {
        const request = requestArray.filter((elmt: FilesDataInterface) => elmt.name === file.requestName)[0];
        // do nothing if the request name in conf file is not present in the schema
        if (!request) continue;
        request.errors = file.errors ? file.errors : [];
        folderData.files.push(request);
      }

      requestsData.folders.push(folderData);
    }

    // get the requests not specified in the json config
    const rest = [...requestArray];

    for (let i = 0; i < requestArray.length; i++) {
      const request = requestArray[i];

      for (const folder of requestsData.folders) {
        const isPresent = folder.files.find((elmt) => {
          return elmt.name === request.name;
        });

        if (!isPresent) continue;

        const index = rest.findIndex((elmt) => {
          return elmt.name === isPresent.name;
        });

        rest.splice(index, 1);
      }
    }

    requestsData.rest = rest.sort((elmt) => {
      if (elmt.requestType === 'mutation') return 1;
      return -1;
    });
    return requestsData;
  }

  private async formatRequestData(fileContent: string, requestType: string, conf: ConfInterface): Promise<FilesDataInterface[]> {
    const splitedContent: string = fileContent.split(/{(\n|\r\n)/)[2].split(/}(\n|\r\n)/)[0];
    const requests = splitedContent.split(/(\n|\r\n)/).filter((elmt) => !/(\n|\r\n|^$)/g.test(elmt));

    const tmpArray: FilesDataInterface[] = [];

    const lastDescription = {
      description: '',
      request: '',
    };

    // formatting the data
    for (let i = 0; i < requests.length; i++) {
      const request: string = requests[i];

      if (request.trim().slice(0, 3) === '"""') {
        lastDescription.description = request.replace(/"""/g, '').trim();
        lastDescription.request = requests[i + 1];
        continue;
      }

      let requestName: string;
      let requestInput: string | undefined;
      let requestOutput: string;
      let inputName: string | undefined;

      // check if request have input or not
      if (request.includes('(')) {
        requestName = request.split('(')[0];
        inputName = request.split('(')[1].split(':')[0];
        requestInput = request.split(`(${inputName}:`)[1].split(')')[0];
        requestOutput = request.split('):')[1];
      } else {
        requestName = request.split(':')[0];
        requestOutput = request.split(':')[1];
      }

      let inputObject: InputDataInterface | null = null;
      let outputObject: OutputDataInterface | string | null = null;
      // add input and output
      if (requestInput && requestInput.split('=')[0]) inputObject = await this.getInput(requestInput.split('=')[0], conf);

      if (requestOutput) outputObject = await this.getOutput(requestOutput.trim(), false, conf);

      const requestDataObject: FilesDataInterface = {
        name: requestName.trim(),
        input: inputObject,
        inputName: inputName ? inputName : null,
        output: outputObject,
        description: lastDescription.request === request ? lastDescription.description : null,
        requestType: requestType,
        errors: [],
      };

      tmpArray.push(requestDataObject);
    }

    return tmpArray;
  }

  private async getInput(input: string, conf: ConfInterface): Promise<InputDataInterface | null> {
    const inputName: string = input.trim().replace(/!/g, '').replace(/\[|\]/g, '');
    const isOptionable: boolean = input.slice(-1) === '!' ? false : true;

    const path = `${join(conf.output.path, conf.output.dirName)}/assets/inputs/${inputName}.md`;

    if (!existsSync(path)) {
      return null;
    }

    const fileContent = await promises.readFile(path, 'utf8');

    const inputDef: string[] = fileContent
      .split('{')[1]
      .split('}')[0]
      .split(/(\n|\r\n)/)
      .filter((elmt) => !/(\n|\r\n|^$)/g.test(elmt));

    const def: InputDefInterface[] = [];

    const lastDescription = {
      description: '',
      input: '',
    };

    // loop on all the input properties
    for (let i = 0; i < inputDef.length; i++) {
      const prop: string = inputDef[i];

      // split the name from the string
      const splitedProp = prop.split(':');
      const propName = splitedProp[0].trim();
      const [, ...rest] = [...splitedProp];
      const restString: string = rest.join(':').trim();

      let propType: string = restString.split(' ')[0];

      // get the deprecated reason if there is one
      let deprecated: string | undefined = restString.split(/@deprecated\(reason: /)[1];
      if (deprecated !== undefined) deprecated = deprecated.replace(/"|\)/g, '');

      // get the default value if there is one and remove it from the type
      let propDefault = '';
      if (propType) {
        propType = propType.split('=')[0].trim();
        propDefault = prop.split(':')[1].split('=')[1];
        // remove all after default value
        if (propDefault) propDefault = propDefault.trim().split(' ')[0];
      }

      // check if it is a description and save it
      if (prop.trim().slice(0, 3) === '"""') {
        lastDescription.description = prop;
        lastDescription.input = inputDef[i + 1].split(':')[0].trim();
        continue;
      }

      const property: InputDefInterface = {
        name: propName,
        type: propType,
        default: propDefault ? propDefault.trim() : '',
        description: lastDescription.input === propName ? lastDescription.description.replace(/"""/g, '').trim() : undefined,
        deprecated: deprecated,
        def: [],
      };

      const subInput = await this.getInput(propType, conf);

      if (subInput) {
        property.def.push(subInput);
      }
      def.push(property);
    }

    const inputObj: InputDataInterface = {
      name: inputName,
      optionable: isOptionable,
      def: def,
    };

    return inputObj;
  }

  private async getOutput(output: string, secondCall: boolean, conf: ConfInterface, depth = 0, parentTree: any = []): Promise<OutputDataInterface | string | null> {
    if (depth > this.maxDepth) return 'null';

    const outputName: string = output.replace(/!/g, '').replace(/\[|\]/g, '');

    const isArray: boolean = output.replace(/!/g, '').slice(-1) === ']' && output.replace(/!/g, '').slice(0, 1) === '[' ? true : false;

    let path = `${join(conf.output.path, conf.output.dirName)}/assets`;

    const isOptionable = output.slice(-1) === '!' ? false : true;

    // check if output is a type file
    const isTypeFile: boolean = existsSync(`${path}/types/${outputName}.md`);

    // check if output is an interface file
    const isInterfaceFile: boolean = existsSync(`${path}/interfaces/${outputName}/${outputName}.md`);

    let isSubInterfaceFile = false;
    let interfaceFolderName = 'null';

    // check if output is a subInterface
    if (!isTypeFile && !isInterfaceFile) {
      const interfaceFolders = await promises.readdir(`${path}/interfaces/`);
      for (let i = 0; i < interfaceFolders.length; i++) {
        interfaceFolderName = interfaceFolders[i];
        isSubInterfaceFile = existsSync(`${path}/interfaces/${interfaceFolderName}/${outputName}.md`);
        if (isSubInterfaceFile === true) break;
      }
    }

    // if no file found for the output return it
    if (!isTypeFile && !isInterfaceFile && !isSubInterfaceFile) {
      return secondCall ? null : output;
    }

    const implementations: ImplementationsDataInterface[] = [];

    // change path
    if (isInterfaceFile) path += `/interfaces/${outputName}`;
    if (isTypeFile) path += '/types';
    if (isSubInterfaceFile) path += `/interfaces/${interfaceFolderName}`;

    // get the definition for the interface's implementations
    if (isInterfaceFile) {
      const files = await promises.readdir(path);

      // loop on each interface's implementation
      for (let i = 0; i < files.length; i++) {
        const file = files[i];

        // avoid the base interface, just get the implementations
        if (file.replace('.md', '') === outputName) continue;

        const fileContent = await promises.readFile(`${path}/${file}`, 'utf8');

        const splitedFile = fileContent.split(/ /);
        const fileName = splitedFile[splitedFile.findIndex((elmt) => elmt.includes('type') || elmt.includes('interface')) + 1];

        const def: OutputDefInterface[] = await this.createDefinition(fileContent, conf, depth, parentTree, outputName);

        const implementation: ImplementationsDataInterface = {
          name: fileName,
          def: def,
        };

        implementations.push(implementation);
      }
    }

    // get the type definition
    const fileContent = await promises.readFile(`${path}/${outputName}.md`, 'utf8');
    const def: OutputDefInterface[] = await this.createDefinition(fileContent, conf, depth, parentTree, outputName);

    // remove field in implementation that are present in the interface's definition
    for (let i = 0; i < implementations.length; i++) {
      const implementation = implementations[i];

      const filteredImpl = implementation.def.filter((elmtImpl) => {
        const isPresent = def.find((elmtDef) => elmtDef.name === elmtImpl.name);
        if (!isPresent) return elmtImpl;
      });
      implementations[i].def = filteredImpl;
    }

    const outputObj: OutputDataInterface = {
      name: outputName,
      optionable: isOptionable,
      isArray: isArray,
      def: def,
      implementations: implementations,
    };

    return outputObj;
  }

  private async createDefinition(fileContent: string, conf: ConfInterface, depth: number, parentTree: any, parentName: string): Promise<OutputDefInterface[]> {
    const savedTree = [...parentTree];
    parentTree.push(parentName);

    const outputDef: string[] = fileContent
      .split('{')[1]
      .split('}')[0]
      .split(/(\n|\r\n)/)
      .filter((elmt) => !/(\n|\r\n|^$)/g.test(elmt));

    const def: OutputDefInterface[] = [];

    const lastDescription = {
      description: '',
      output: '',
    };

    for (let i = 0; i < outputDef.length; i++) {
      const prop = outputDef[i];

      // get the description if it exist
      if (prop.trim().slice(0, 3) === '"""') {
        lastDescription.description = prop;
        lastDescription.output = outputDef[i + 1].split(':')[0].trim();
        continue;
      }

      // get prop name from the string
      const splitedProp = prop.split(':');
      let propName = splitedProp[0].trim();
      const [, ...rest] = [...splitedProp];
      const restString: string = rest.join(':').trim();

      let propDef = restString.split(' ')[0];

      // get the deprecated reason if there is one
      let deprecated: string | undefined = restString.split(/@deprecated\(reason: /)[1];
      if (deprecated !== undefined) deprecated = deprecated.replace(/"|\)/g, '');

      // get the subOutput definition if the file exist
      propDef = propDef.trim();

      if (parentTree.includes(propDef.replace(/!/g, '').replace(/\[|\]/g, ''))) continue;

      const subOutput: string | OutputDataInterface | null = await this.getOutput(propDef, true, conf, depth + 1, parentTree);

      // reset the parentTree
      parentTree = [...savedTree, parentName];

      // convert lenght prop to lgth so we can create the array property
      if (propName === 'length') propName = 'lgth';

      const property: OutputDefInterface = {
        name: propName,
        type: propDef,
        description: lastDescription.output === propName ? lastDescription.description.replace(/"""/g, '').trim() : undefined,
        deprecated: deprecated,
        def: [],
      };

      // add sub output if it exist
      if (subOutput) {
        property.def.push(subOutput);
      }

      if (subOutput !== 'null') def.push(property);
    }
    return def;
  }
}
