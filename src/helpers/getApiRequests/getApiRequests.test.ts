import * as fs from 'fs';
import { promises } from 'fs';

import { GetApiRequests } from './getApiRequests';
import { OutputDataInterface } from './requestData.interface';
import {
  conf,
  confTree,
  confTreeError,
  implString,
  implStringWithDepr,
  inputString,
  inputStringWithDepr,
  mutationNoOutput,
  mutationObject,
  mutationObjectInput,
  mutationObjectInputWithDepr,
  mutationObjectNoOutput,
  mutationObjectNotFound,
  mutationObjectOutput,
  mutationObjectOutputInterface,
  mutationObjectOutputInterfaceWithDepr,
  mutationObjectOutputSub,
  mutationObjectSubInput,
  mutationOutputString,
  mutationString,
  outputString,
  outputStringDirect,
  queryObject,
  queryObject2,
  queryObjectError,
  queryString,
  queryStringSort,
  subscriptionObject,
} from './getApiRequests.test.data';

describe('GetApiRequests', () => {
  const getApiRequests = new GetApiRequests();
  const spyRead = jest.spyOn(promises, 'readFile');
  const spyExist = jest.spyOn(fs, 'existsSync');
  const spyReadDir = jest.spyOn(promises, 'readdir');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  it('Should call only existsSync', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(false);
    // act
    await getApiRequests.execute(conf);
    // assert
    expect(spyExist).toHaveBeenCalledTimes(3);
    expect(spyRead).toHaveBeenCalledTimes(0);
  });

  it('Should call existsSync and readFile and return the query in rest', async () => {
    // arrange
    spyExist.mockReturnValueOnce(true); // check query file
    spyExist.mockReturnValueOnce(false); // check muattion file
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(queryString); // get the query file
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    // act
    const result = await getApiRequests.execute(conf);
    // assert
    // query file, muation file, output file , suboutput check => 4 calls
    expect(spyExist).toHaveBeenCalledTimes(5);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.rest[0]).toStrictEqual(queryObject);
  });

  it('Should call existsSync and readFile and return the query in tree with errors list', async () => {
    // arrange
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false); // check muattion file
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(queryString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    // act
    const result = await getApiRequests.execute(confTreeError);
    // assert
    // query file, muation file, output file , suboutput check => 4 calls
    expect(spyExist).toHaveBeenCalledTimes(5);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.folders[0].files[0]).toStrictEqual(queryObjectError);
  });

  it('Should call existsSync and readFile and return the query in tree without errors list', async () => {
    // arrange
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false); // check muattion file
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(queryString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    // act
    const result = await getApiRequests.execute(confTree);
    // assert
    // query file, muation file, output file , suboutput check => 4 calls
    expect(spyExist).toHaveBeenCalledTimes(5);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.folders[0].files[0]).toStrictEqual(queryObject);
  });

  it('Should call existsSync and readFile and not return the mutation in tree if the name in tree prop is not the same', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    // act
    const result = await getApiRequests.execute(confTree);
    // assert
    // query file mutation file, input file, output file, suboutput check => 5 calls
    expect(spyExist).toHaveBeenCalledTimes(6);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.folders[0].files[0]).toStrictEqual(undefined);
  });

  it('Should call existsSync and readFile and return the mutation in rest', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    // act
    const result = await getApiRequests.execute(conf);
    // assert
    // query file mutation file, input file, output file, suboutput check => 5 calls
    expect(spyExist).toHaveBeenCalledTimes(6);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.rest[0]).toStrictEqual(mutationObject);
  });

  it('Should return a query and a mutation', async () => {
    // arrange
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(queryStringSort);
    spyRead.mockResolvedValueOnce(mutationString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    // act
    const result = await getApiRequests.execute(conf);
    // assert
    // query file mutation file, input file, 3 output file, 3 suboutput check => 9 calls
    expect(spyExist).toHaveBeenCalledTimes(10);
    expect(spyRead).toHaveBeenCalledTimes(2);
    expect(spyReadDir).toHaveBeenCalledTimes(3);
    expect(result.rest[0]).toStrictEqual(queryObject2);
    expect(result.rest[1]).toStrictEqual(queryObject);
    expect(result.rest[2]).toStrictEqual(mutationObject);
  });

  it('Should return a mutation with an input', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // get input
    spyExist.mockReturnValueOnce(true);
    spyRead.mockResolvedValueOnce(inputString);
    // act
    const result = await getApiRequests.execute(conf);
    // assert
    expect(spyExist).toHaveBeenCalledTimes(8);
    expect(spyRead).toHaveBeenCalledTimes(2);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.rest[0]).toStrictEqual(mutationObjectInput);
  });

  it('Should return a mutation with an input and deprecated fields', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // get input
    spyExist.mockReturnValueOnce(true);
    spyRead.mockResolvedValueOnce(inputStringWithDepr);
    // act
    const result = await getApiRequests.execute(conf);
    // assert
    expect(spyExist).toHaveBeenCalledTimes(8);
    expect(spyRead).toHaveBeenCalledTimes(2);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.rest[0]).toStrictEqual(mutationObjectInputWithDepr);
  });

  it('Should return a mutation with an input containing a subInput', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationString);

    // get input
    spyExist.mockReturnValueOnce(true);
    spyRead.mockResolvedValueOnce(inputString);

    // get subInput
    spyExist.mockReturnValueOnce(true);
    spyRead.mockResolvedValueOnce(inputString);

    // get output
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // act
    const result = await getApiRequests.execute(conf);

    // assert
    expect(spyExist).toHaveBeenCalledTimes(10);
    expect(spyRead).toHaveBeenCalledTimes(3);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.rest[0].input!.def[0]).toStrictEqual(mutationObjectSubInput.input!.def[0]);
  });

  it('Should return a mutation with an output interface with an implementation', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationOutputString);

    // get input
    spyExist.mockReturnValueOnce(false);

    // check output
    spyExist.mockReturnValueOnce(false); // isTypeFile
    spyExist.mockReturnValueOnce(true); // isInterface file

    // get implementation
    spyReadDir.mockResolvedValueOnce(['test', 'UserInterface' as any]);
    spyRead.mockResolvedValueOnce(implString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // get output
    spyRead.mockResolvedValueOnce(outputString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // get  second output
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // act
    const result = await getApiRequests.execute(conf);
    // assert
    expect(spyExist).toHaveBeenCalledTimes(14);
    expect(spyRead).toHaveBeenCalledTimes(3);
    expect(spyReadDir).toHaveBeenCalledTimes(5);
    expect(result.rest[0]).toStrictEqual(mutationObjectOutputInterface);
  });

  it('Should return a mutation with an output interface with an implementation', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationOutputString);

    // get input
    spyExist.mockReturnValueOnce(false);

    // check output
    spyExist.mockReturnValueOnce(false); // isTypeFile
    spyExist.mockReturnValueOnce(true); // isInterface file

    // get implementation
    spyReadDir.mockResolvedValueOnce(['test', 'UserInterface' as any]);
    spyRead.mockResolvedValueOnce(implStringWithDepr);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // get output
    spyRead.mockResolvedValueOnce(outputString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // get  second output
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // act
    const result = await getApiRequests.execute(conf);
    // assert
    expect(spyExist).toHaveBeenCalledTimes(14);
    expect(spyRead).toHaveBeenCalledTimes(3);
    expect(spyReadDir).toHaveBeenCalledTimes(5);
    expect(result.rest[0]).toStrictEqual(mutationObjectOutputInterfaceWithDepr);
  });

  it('Should return a mutation with an output with a subOutput', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationOutputString);

    // get input
    spyExist.mockReturnValueOnce(false);

    // check output
    spyExist.mockReturnValueOnce(true); // isTypeFile
    spyExist.mockReturnValueOnce(false); // isInterface file
    spyRead.mockResolvedValueOnce(outputString);

    // get subOutpub
    spyExist.mockReturnValueOnce(true); // isTypeFile
    spyExist.mockReturnValueOnce(false); // isInterface file
    spyRead.mockResolvedValueOnce(outputString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // act
    const result = await getApiRequests.execute(conf);
    // assert
    const output = result.rest[0].output as OutputDataInterface;
    const expected: OutputDataInterface = mutationObjectOutputSub.output as OutputDataInterface;
    expect(spyExist).toHaveBeenCalledTimes(8);
    expect(spyRead).toHaveBeenCalledTimes(3);
    expect(spyReadDir).toHaveBeenCalledTimes(0);
    expect(output.def[0]).toStrictEqual(expected.def[0]);
  });

  it('Should return a mutation with an output with a subOutput and a directive', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationOutputString);

    // get input
    spyExist.mockReturnValueOnce(false);

    // check output
    spyExist.mockReturnValueOnce(true); // isTypeFile
    spyExist.mockReturnValueOnce(false); // isInterface file
    spyRead.mockResolvedValueOnce(outputStringDirect);

    // get subOutpub
    spyExist.mockReturnValueOnce(true); // isTypeFile
    spyExist.mockReturnValueOnce(false); // isInterface file
    spyRead.mockResolvedValueOnce(outputString);
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // act
    const result = await getApiRequests.execute(conf);
    // assert
    const output = result.rest[0].output as OutputDataInterface;
    const expected: OutputDataInterface = mutationObjectOutputSub.output as OutputDataInterface;
    expect(spyExist).toHaveBeenCalledTimes(8);
    expect(spyRead).toHaveBeenCalledTimes(3);
    expect(spyReadDir).toHaveBeenCalledTimes(0);
    expect(output.def[0]).toStrictEqual(expected.def[0]);
  });

  it('Should return a mutation without an output', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationNoOutput);

    // get input
    spyExist.mockReturnValueOnce(false);

    // act
    const result = await getApiRequests.execute(conf);
    // assert
    expect(spyExist).toHaveBeenCalledTimes(4);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(result.rest[0]).toStrictEqual(mutationObjectNoOutput);
  });

  it('Should return a mutation with an output subInterface', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationOutputString);

    // get input
    spyExist.mockReturnValueOnce(false);

    // check output
    spyExist.mockReturnValueOnce(false); // isTypeFile
    spyExist.mockReturnValueOnce(false); // isInterface file
    spyReadDir.mockResolvedValueOnce(['interfaceFolder' as any]); // get the list of interface folder
    spyExist.mockReturnValueOnce(true); // check if the subInterfaceFile exist
    spyRead.mockResolvedValueOnce(outputString); // get the subInterfaceFile content
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder

    // act
    const result = await getApiRequests.execute(conf);
    // assert
    expect(spyExist).toHaveBeenCalledTimes(9);
    expect(spyRead).toHaveBeenCalledTimes(2);
    expect(spyReadDir).toHaveBeenCalledTimes(2);
    expect(result.rest[0]).toStrictEqual(mutationObjectOutput);
  });

  it('Should return a mutation without an output subInterface', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false);
    spyExist.mockReturnValueOnce(true);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(mutationOutputString);

    // get input
    spyExist.mockReturnValueOnce(false);

    // check output
    spyExist.mockReturnValueOnce(false); // isTypeFile
    spyExist.mockReturnValueOnce(false); // isInterface file
    spyReadDir.mockResolvedValueOnce(['interfaceFolder' as any]); // get the list of interface folder
    spyExist.mockReturnValueOnce(false); // check if the subInterfaceFile exist

    // act
    const result = await getApiRequests.execute(conf);
    // assert
    expect(spyExist).toHaveBeenCalledTimes(7);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.rest[0]).toStrictEqual(mutationObjectNotFound);
  });

  it('Should call existsSync and readFile and return the subscription', async () => {
    // arrange
    spyExist.mockReturnValueOnce(false); // check query file
    spyExist.mockReturnValueOnce(false); // check muattion file
    spyExist.mockReturnValueOnce(true);
    spyRead.mockResolvedValueOnce(queryString); // get the subscription file
    spyReadDir.mockResolvedValueOnce([]); // get the list of interface folder
    // act
    const result = await getApiRequests.execute(conf);
    // assert
    // query file, muation file, output file , suboutput check => 4 calls
    expect(spyExist).toHaveBeenCalledTimes(5);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(spyReadDir).toHaveBeenCalledTimes(1);
    expect(result.rest[0]).toStrictEqual(subscriptionObject);
  });
});
