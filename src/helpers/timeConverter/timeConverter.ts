const MINUTES = 60;
const HOURS = 60 * MINUTES;
const DAYS = 24 * HOURS;

export class TimeConverter {
  static convert(ms: number): string {
    const difference = ms / 1000;

    const diff = {
      hours: Math.floor((difference % DAYS) / HOURS),
      minutes: Math.floor((difference % HOURS) / MINUTES),
      seconds: Math.floor(difference % MINUTES),
      milliseconds: Math.floor(ms),
    };

    let message = '';
    let separator = '';

    if (diff.hours >= 1) {
      message += `${diff.hours}h`;
      separator = ' ';
    }
    if (diff.minutes >= 1) {
      message += `${separator}${diff.minutes}m`;
      separator = ' ';
    }
    if (diff.seconds >= 1) {
      message += `${separator}${diff.seconds}s`;
    }
    if (diff.milliseconds && diff.seconds < 1 && diff.minutes < 1 && diff.hours < 1) {
      message += `${diff.milliseconds}ms`;
    }

    return message;
  }
}
