import { TimeConverter } from './timeConverter';

describe('TimeConverter', () => {
  it('should show milliseconds', () => {
    const ms = 38.254;
    const duration = TimeConverter.convert(ms);
    expect(duration).toEqual('38ms');
  });

  it('should convert milliseconds to secondes', () => {
    const ms = 1630;
    const duration = TimeConverter.convert(ms);

    expect(duration).toEqual('1s');
  });

  it('should convert milliseconds to minutes', () => {
    const ms = 60 * 1440;
    const duration = TimeConverter.convert(ms);

    expect(duration).toEqual('1m 26s');
  });

  it('should convert milliseconds to hours', () => {
    const ms = 60 * 60 * 9020;
    const duration = TimeConverter.convert(ms);

    expect(duration).toEqual('9h 1m 12s');
  });
});
