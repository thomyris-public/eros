import * as fs from 'fs';
import { GenerateFromType } from './generateFromType';
import { ConfInterface } from '../getJsonConfig/conf.interface';
import { GenFileHandler } from '../genFileHandler/genFileHandler';
import { GenFileDatasInterface } from '../genFileHandler/genFile.Interface';

describe('GenerateFromType', () => {
  const handleGenFile = new GenFileHandler();
  const generateFromType = new GenerateFromType(handleGenFile);
  const spyRand = jest.spyOn(global.Math, 'random');

  const spyWriteGenFile = jest.spyOn(handleGenFile, 'write');
  const spyGetType = jest.spyOn(handleGenFile, 'getType');
  const spyExistsSync = jest.spyOn(fs, 'existsSync');
  const spyReadFile = jest.spyOn(fs.promises, 'readFile');

  spyWriteGenFile.mockResolvedValue();

  const conf: ConfInterface = {
    input: 'input',
    output: {
      path: './',
      dirName: '',
    },
    maxDepth: 2,
    app: {
      title: 'app',
      description: 'description',
    },
    scalarDefinitions: [
      {
        name: 'test',
        regex: '^abc$',
      },
    ],
  };

  const confNoScalar: ConfInterface = {
    input: 'input',
    output: {
      path: './',
      dirName: '',
    },
    maxDepth: 2,
    app: {
      title: 'app',
      description: 'description',
    },
  };

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should return the type because nothing match it', async () => {
    // arrange
    spyGetType.mockResolvedValueOnce(undefined);
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await generateFromType.execute('noMatch', conf);
    // assert
    expect(result).toStrictEqual('noMatch');
  });

  it('Should return a generated Float', async () => {
    // arrange
    spyRand.mockReturnValueOnce(0.111);
    spyGetType.mockResolvedValueOnce(undefined);
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await generateFromType.execute('Float', conf);
    // assert
    expect(result).toStrictEqual(11.1);
  });

  it('Should return a generated String', async () => {
    // arrange
    spyRand.mockReturnValueOnce(0.111);
    spyGetType.mockResolvedValueOnce(undefined);
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await generateFromType.execute('String', conf);
    // assert
    expect(result).toStrictEqual('"3zutdjanm6"');
  });

  it('Should return a generated Boolean', async () => {
    // arrange
    spyRand.mockReturnValueOnce(0.4);
    spyGetType.mockResolvedValueOnce(undefined);
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await generateFromType.execute('Boolean', conf);
    // assert
    expect(result).toStrictEqual(true);
  });

  it('Should return a generated Integer', async () => {
    // arrange
    spyRand.mockReturnValueOnce(0.44444);
    spyGetType.mockResolvedValueOnce(undefined);
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await generateFromType.execute('Int', conf);
    // assert
    expect(result).toStrictEqual(44);
  });

  it('Should return a generated ID', async () => {
    // arrange
    spyRand.mockReturnValueOnce(0.111);
    spyGetType.mockResolvedValueOnce(undefined);
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await generateFromType.execute('ID', conf);
    // assert
    expect(result).toStrictEqual('"tdjanm6"');
  });

  it('Should return a generated string for the scalar', async () => {
    // arrange
    spyGetType.mockResolvedValueOnce(undefined);
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await generateFromType.execute('test', conf);
    // assert
    expect(result).toStrictEqual('"abc"');
  });

  it('Should return a generated string no scalar', async () => {
    // arrange
    spyGetType.mockResolvedValueOnce(undefined);
    spyWriteGenFile.mockResolvedValue();
    // act
    const result = await generateFromType.execute('test', confNoScalar);
    // assert
    expect(result).toStrictEqual('test');
  });

  it('Should not call write from GenFile because value for type already generated', async () => {
    // arrange
    const objectType: GenFileDatasInterface = {
      type: 'test',
      value: 'test value',
    };

    spyGetType.mockResolvedValueOnce(objectType);
    // act
    const result = await generateFromType.execute('test', confNoScalar);
    // assert
    expect(result).toStrictEqual('"test value"');
  });

  it('Should call write from GenFile because regex for type already generated has changed', async () => {
    // arrange
    const objectType: GenFileDatasInterface = {
      type: 'test',
      value: 'test value',
      regex: 'test',
    };

    spyGetType.mockResolvedValueOnce(objectType);
    // act
    const result = await generateFromType.execute('test', conf);
    // assert
    expect(result).toStrictEqual('"abc"');
  });

  it('Should return a value for an enum', async () => {
    // arrange
    spyGetType.mockResolvedValueOnce(undefined);
    spyExistsSync.mockReturnValueOnce(true);
    spyReadFile.mockResolvedValueOnce('ouep{\n TEST');
    // act
    const result = await generateFromType.execute('enum', conf);
    // assert
    expect(result).toStrictEqual('ouep{');
  });
});
