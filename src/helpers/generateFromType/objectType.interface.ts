export interface ObjectTypeInterface {
  type: string;
  regex?: string;
}
