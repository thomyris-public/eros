import * as RandExp from 'randexp';
import { existsSync, promises } from 'fs';

import { gqlBuiltInType } from '../enums/gqlBuiltInType';
import { ObjectTypeInterface } from './objectType.interface';
import { GenFileHandlerInterface } from '../genFileHandler/genFileHandler';
import { GenFileDatasInterface } from '../genFileHandler/genFile.Interface';
import { ConfInterface, ScalarConfInterface } from '../getJsonConfig/conf.interface';

export interface GenerateFromTypeInterface {
  execute(type: string, conf: ConfInterface): Promise<string | number | boolean>;
}

export class GenerateFromType implements GenerateFromTypeInterface {
  private handleGenFile: GenFileHandlerInterface;

  constructor(handleGenFile: GenFileHandlerInterface) {
    this.handleGenFile = handleGenFile;
  }

  async execute(type: string, conf: ConfInterface): Promise<string | number | boolean> {
    type = type.replace(/!|\[|\]/g, '').trim();

    const scalarType: ScalarConfInterface | undefined = conf.scalarDefinitions ? conf.scalarDefinitions.find((scalar) => scalar.name === type) : undefined;

    const objectType: GenFileDatasInterface | undefined = await this.handleGenFile.getType(type);

    let generatedString: string | number | boolean = type;

    if (gqlBuiltInType[type]) {
      switch (type) {
        case 'Float':
          const floatValue: number = Math.random() * 100;
          generatedString = floatValue;
          break;

        case 'String':
          const stringValue: string = (Math.random() + 1).toString(36).substring(2);
          generatedString = `${stringValue}`;
          break;

        case 'Boolean':
          const booleanValue: boolean = Math.random() < 0.5;
          generatedString = booleanValue;
          break;

        case 'Int':
          const intValue: number = Math.floor(Math.random() * 100);
          generatedString = intValue;
          break;

        case 'ID':
          const idValue: string = (Math.random() + 1).toString(36).substring(5);
          generatedString = `${idValue}`;
          break;
      }
    }

    const newObjectType: ObjectTypeInterface = {
      type: type,
      regex: scalarType?.regex,
    };

    // get the regex specified in the conf file
    if (scalarType) {
      generatedString = `${new RandExp(scalarType.regex).gen()}`;
    }

    const path: string = conf.output.path + conf.output.dirName + '/assets/enums/' + type.toLowerCase() + '.md';
    const isEnum: boolean = existsSync(path);

    if (isEnum === true) {
      const file: string = await promises.readFile(path, 'utf8');
      const splitedFile: string[] = file.split(/(?<=\n)/);
      const value: number = splitedFile.findIndex((line: string) => {
        if (line.toUpperCase().includes(type.toUpperCase())) return true;
      });
      generatedString = splitedFile[value + 1].trim().replace(/(\r\n|\n|\r)/gm, '');
    }

    if (objectType) {
      const isSameRegex: boolean = objectType.regex === scalarType?.regex;
      if (!isSameRegex) {
        await this.handleGenFile.write(newObjectType, generatedString);
        generatedString = generatedString;
      } else {
        generatedString = objectType.value;
      }
    }

    await this.handleGenFile.write(newObjectType, generatedString);
    if (typeof generatedString === 'string' && generatedString !== type.replace(/!|\[|\]/g, '').trim() && isEnum === false) generatedString = `"${generatedString}"`;
    return generatedString;
  }
}
