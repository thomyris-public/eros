import { existsSync, promises } from 'fs';

import { ConfInterface } from './conf.interface';

export interface GetJsonConfigInterface {
  confFileName: string;
  execute(): Promise<ConfInterface>;
}

export class GetJsonConfig implements GetJsonConfigInterface {
  public confFileName = 'eros.conf.json';

  public execute = async (): Promise<ConfInterface> => {
    // check if file exists
    if (!existsSync(`./${this.confFileName}`)) {
      throw new Error('Conf file eros.conf.json not found');
    }

    // get the conf and parse it
    let conf: ConfInterface;
    const file: string = await promises.readFile(`./${this.confFileName}`, 'utf8');
    try {
      conf = JSON.parse(file);
      if (conf.maxDepth === undefined) conf.maxDepth = 2;
    } catch (error) {
      throw new Error(`./${this.confFileName} is not a valid Json`);
    }

    // check the conf
    this.checkConf(conf);
    return conf;
  };

  // Check if the conf file is properly writen
  private checkConf = (conf: ConfInterface): void => {
    if (!conf.input) throw new Error(`${this.confFileName} file need an input property to define the location of the graphql schema file`);
    if (!conf.output) throw new Error(`${this.confFileName} file need an output property to define where the documentation will be generated`);

    // check output
    if (!conf.output.path) throw new Error(`${this.confFileName} file need an output.path property to define where the documentation will be generated`);
    if (conf.output.dirName === undefined) throw new Error(`${this.confFileName} file need an output.dirName property to define the name of the generated directory`);

    // check app
    if (conf.app) {
      if (!conf.app.title) throw new Error(`${this.confFileName} file need an app.title property to define your app title`);
    } else throw new Error(`${this.confFileName} file need an app property to define your app title and description`);

    // check tree and files
    if (conf.tree) {
      for (const folderName in conf.tree) {
        const folder = conf.tree[folderName];

        for (let i = 0; i < folder.length; i++) {
          const file = folder[i];
          if (!file.requestName) throw new Error(`${this.confFileName} a file need a requestName property`);
          if (file.errors) {
            for (let i = 0; i < file.errors.length; i++) {
              const error = file.errors[i];
              if (!error.name) throw new Error('An error need a name');
            }
          }
        }
      }
    }

    // check scalarDefinitions
    if (conf.scalarDefinitions) {
      for (let i = 0; i < conf.scalarDefinitions.length; i++) {
        const scalar = conf.scalarDefinitions[i];
        if (!scalar.name) throw new Error(`${this.confFileName} a scalar need a name property`);
        if (!scalar.regex) throw new Error(`${this.confFileName} a scalar need a regex property`);
      }
    }
  };
}
