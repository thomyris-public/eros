import { ConfInterface } from './conf.interface';

const stringInput = JSON.stringify({
  test: 'test',
});

const stringOutput = JSON.stringify({
  input: 'test',
});

const stringOutputPath = JSON.stringify({
  input: 'in',
  output: {},
});

const stringOutputDirName = JSON.stringify({
  input: 'in',
  output: {
    path: './',
  },
});

const stringApp = JSON.stringify({
  input: 'in',
  output: {
    path: './',
    dirName: '',
  },
});

const stringAppTitle = JSON.stringify({
  input: 'in',
  output: {
    path: './',
    dirName: '',
  },
  app: {
    test: 'test',
  },
});

const confTreeScalars = {
  input: 'in',
  output: {
    path: './',
    dirName: '',
  },
  maxDepth: 2,
  app: {
    title: 'title',
  },
};

const stringTreeScalars = JSON.stringify(confTreeScalars);

const stringTreeFileName = JSON.stringify({
  input: 'in',
  output: {
    path: './',
    dirName: '',
  },
  app: {
    title: 'title',
  },
  tree: {
    vpn: [
      {
        test: 'test',
      },
    ],
  },
});

const stringTreeFileErrorName = JSON.stringify({
  input: 'in',
  output: {
    path: './',
    dirName: '',
  },
  app: {
    title: 'title',
  },
  tree: {
    vpn: [
      {
        requestName: 'vpnServers',
        errors: [
          {
            reason: 'no name',
          },
        ],
      },
    ],
  },
});

const stringScalarsName = JSON.stringify({
  input: 'in',
  output: {
    path: './',
    dirName: '',
  },
  app: {
    title: 'title',
  },
  tree: {
    vpn: [
      {
        requestName: 'vpnServers',
        errors: [
          {
            name: 'Error',
            reason: 'no reason',
          },
        ],
      },
    ],
  },
  scalarDefinitions: [
    {
      test: 'test',
    },
  ],
});

const stringScalarsRegex = JSON.stringify({
  input: 'in',
  output: {
    path: './',
    dirName: '',
  },
  app: {
    title: 'title',
  },
  tree: {
    vpn: [
      {
        requestName: 'vpnServers',
      },
    ],
  },
  scalarDefinitions: [
    {
      name: 'name',
      test: 'test',
    },
  ],
});

const fullConf: ConfInterface = {
  input: 'in',
  output: {
    path: './',
    dirName: '',
  },
  maxDepth: 2,
  app: {
    title: 'title',
  },
  tree: {
    vpn: [
      {
        requestName: 'vpnServers',
      },
    ],
  },
  scalarDefinitions: [
    {
      name: 'scalarName',
      regex: 'rege',
    },
  ],
};

const fullString = JSON.stringify(fullConf);

export {
  stringInput,
  stringOutput,
  stringApp,
  stringAppTitle,
  stringTreeScalars,
  confTreeScalars,
  stringTreeFileName,
  stringScalarsName,
  stringScalarsRegex,
  fullString,
  fullConf,
  stringTreeFileErrorName,
  stringOutputPath,
  stringOutputDirName,
};
