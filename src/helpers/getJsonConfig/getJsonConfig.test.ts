import * as fs from 'fs';
import { promises } from 'fs';

import { ConfInterface } from './conf.interface';
import { GetJsonConfig } from './getJsonConfig';
import {
  confTreeScalars,
  fullConf,
  fullString,
  stringApp,
  stringAppTitle,
  stringInput,
  stringOutput,
  stringScalarsName,
  stringScalarsRegex,
  stringTreeScalars,
  stringTreeFileName,
  stringTreeFileErrorName,
  stringOutputPath,
  stringOutputDirName,
} from './getJsonConfig.test.data';

describe('index', () => {
  const getConf = new GetJsonConfig();

  const spyFs = jest.spyOn(fs, 'existsSync');
  const spyRead = jest.spyOn(promises, 'readFile');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should call fs existsSync and readFile', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(fullString);
    // act
    await getConf.execute();
    // assert
    expect(spyFs).toHaveBeenCalledTimes(1);
    expect(spyRead).toHaveBeenCalledTimes(1);
  });

  it('Should return the full conf', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(fullString);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(fullConf);
  });

  it('Should return the conf without tree and scalars prop', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringTreeScalars);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(confTreeScalars);
  });

  it('Should throw because file not found', async () => {
    // arrange
    spyFs.mockReturnValue(false);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('Conf file eros.conf.json not found'));
  });

  it('Should throw because the conf file is not a valid Json', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue('');
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('./eros.conf.json is not a valid Json'));
  });

  it('Should throw because the conf file is missing the property input', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringInput);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('eros.conf.json file need an input property to define the location of the graphql schema file'));
  });

  it('Should throw because the conf file is missing the property output', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringOutput);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('eros.conf.json file need an output property to define where the documentation will be generated'));
  });

  it('Should throw because the conf file is missing the property output.path', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringOutputPath);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('eros.conf.json file need an output.path property to define where the documentation will be generated'));
  });

  it('Should throw because the conf file is missing the property output.dirName', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringOutputDirName);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('eros.conf.json file need an output.dirName property to define the name of the generated directory'));
  });

  it('Should throw because the conf file is missing the property app', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringApp);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('eros.conf.json file need an app property to define your app title and description'));
  });

  it('Should throw because the conf file is missing the property app.title', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringAppTitle);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('eros.conf.json file need an app.title property to define your app title'));
  });

  it('Should throw because the conf file is missing the property name for a file in prop tree', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringTreeFileName);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('eros.conf.json a file need a requestName property'));
  });

  it('Should throw because the conf file is missing the property name for a error in file prop tree', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringTreeFileErrorName);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('An error need a name'));
  });

  it('Should throw because the conf file is missing the property name for a scalar', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringScalarsName);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('eros.conf.json a scalar need a name property'));
  });

  it('Should throw because the conf file is missing the property regex for a scalar', async () => {
    // arrange
    spyFs.mockReturnValue(true);
    spyRead.mockResolvedValue(stringScalarsRegex);
    // act
    let result: ConfInterface | any;
    try {
      result = await getConf.execute();
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('eros.conf.json a scalar need a regex property'));
  });
});
