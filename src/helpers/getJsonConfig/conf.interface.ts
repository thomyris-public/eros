export interface ConfInterface {
  input: string;
  output: OutputConfInterface;
  maxDepth: number;
  app: {
    title: string;
    description?: string;
  };
  tree?: TreeConfInterface;
  scalarDefinitions?: ScalarConfInterface[];
}

export interface OutputConfInterface {
  path: string;
  dirName: string;
}

export interface TreeConfInterface {
  [key: string]: FolderConfInterface;
}

/* eslint @typescript-eslint/no-empty-interface: "off" */
export interface FolderConfInterface extends Array<FileConfInterface> {}

export interface FileConfInterface {
  requestName: string;
  errors?: ErrorConfInterface[];
}

export interface ErrorConfInterface {
  name: string;
  reason?: string;
}

export interface ScalarConfInterface {
  name: string;
  regex: string;
}
