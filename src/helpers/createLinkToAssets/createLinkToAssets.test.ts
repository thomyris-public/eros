import * as fs from 'fs';
import { promises } from 'fs';

import { FormatPath } from '../formatPath/formatPath';
import { CreateLinkToAssets } from './createLinkToAssets';
import { GetJsonConfig } from '../getJsonConfig/getJsonConfig';
import { ConfInterface } from '../getJsonConfig/conf.interface';

describe('CreateLinkToAssets', () => {
  const getConf = new GetJsonConfig();
  const formatPath = new FormatPath();
  const createLinkToAssets = new CreateLinkToAssets(getConf, formatPath);
  const spyRead = jest.spyOn(promises, 'readdir');
  const spyExist = jest.spyOn(fs, 'existsSync');
  const spyGetJsonConfig = jest.spyOn(getConf, 'execute');

  const conf: ConfInterface = {
    app: {
      title: 'title',
    },
    maxDepth: 2,
    input: '',
    output: {
      path: './',
      dirName: 'doc',
    },
  };

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should call readdir and exixstsStnc', async () => {
    // arrange
    spyGetJsonConfig.mockResolvedValueOnce(conf);
    spyRead.mockResolvedValueOnce(['testFile' as any]);
    // act
    await createLinkToAssets.execute('test', './test');
    // assert
    expect(spyGetJsonConfig).toHaveBeenCalledTimes(1);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(spyExist).toHaveBeenCalledTimes(1);
  });

  it('Should return a string link', async () => {
    // arrange
    spyGetJsonConfig.mockResolvedValueOnce(conf);
    spyRead.mockResolvedValueOnce(['testFile' as any]);
    spyExist.mockReturnValue(true);
    // act
    const result = await createLinkToAssets.execute('test', './test');
    // assert
    expect(spyGetJsonConfig).toHaveBeenCalledTimes(1);
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(spyExist).toHaveBeenCalledTimes(1);
    expect(result).toStrictEqual('[test](../doc/assets/testFile/test.md)');
  });

  it('Should return a string link for an interface', async () => {
    // arrange
    spyGetJsonConfig.mockResolvedValueOnce(conf);
    spyRead.mockResolvedValueOnce(['interfaces' as any]);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(['testFolder' as any]);
    spyExist.mockReturnValueOnce(true);
    // act
    const result = await createLinkToAssets.execute('test', './test');
    // assert
    expect(spyGetJsonConfig).toHaveBeenCalledTimes(1);
    expect(spyRead).toHaveBeenCalledTimes(2);
    expect(spyExist).toHaveBeenCalledTimes(2);
    expect(result).toStrictEqual('[test](../doc/assets/interfaces/testFolder/test.md)');
  });

  it('Should return the fileName if nothing match', async () => {
    // arrange
    spyGetJsonConfig.mockResolvedValueOnce(conf);
    spyRead.mockResolvedValueOnce(['interfaces' as any]);
    spyExist.mockReturnValueOnce(false);
    spyRead.mockResolvedValueOnce(['test' as any]);
    spyExist.mockReturnValueOnce(false);
    // act
    const result = await createLinkToAssets.execute('test', './test');
    // assert
    expect(spyGetJsonConfig).toHaveBeenCalledTimes(1);
    expect(spyRead).toHaveBeenCalledTimes(2);
    expect(spyExist).toHaveBeenCalledTimes(2);
    expect(result).toStrictEqual('test');
  });
});
