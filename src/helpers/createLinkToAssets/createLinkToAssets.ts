import { existsSync, promises } from 'fs';
import { relative, join, posix, sep } from 'path';
import { FormatPathInterface } from '../formatPath/formatPath';

import { ConfInterface } from '../getJsonConfig/conf.interface';
import { GetJsonConfigInterface } from '../getJsonConfig/getJsonConfig';

export interface CreateLinkToAssetsInterface {
  execute(fileName: string, docFilePath: string): Promise<string>;
}

export class CreateLinkToAssets implements CreateLinkToAssetsInterface {
  private getConf: GetJsonConfigInterface;
  private formatPath: FormatPathInterface;

  constructor(getConf: GetJsonConfigInterface, formatPath: FormatPathInterface) {
    this.getConf = getConf;
    this.formatPath = formatPath;
  }

  async execute(filename: string, docFilePath: string): Promise<string> {
    const conf: ConfInterface = await this.getConf.execute();
    const path = `${join(conf.output.path, conf.output.dirName)}/assets`;
    const assetsFolders: string[] = await promises.readdir(path);

    // loop on each folder in the assets folder
    for (const folder of assetsFolders) {
      const formatedFileName = this.formatPath.execute(filename.replace(/^\[|\]$/g, ''));

      const assetsFolderPath = `${path}/${folder}`;
      const assetsFilePath = `${assetsFolderPath}/${formatedFileName}.md`;

      // return the path to the file if it exist
      if (existsSync(assetsFilePath)) {
        const path = relative(docFilePath, assetsFilePath);
        const linkString = `[${filename}](${path.split(sep).join(posix.sep)})`;
        return linkString;
      }

      if (folder === 'interfaces') {
        const interfaceFolders = await promises.readdir(assetsFolderPath);

        // loop on all interface folder
        for (let i = 0; i < interfaceFolders.length; i++) {
          const interfaceName = interfaceFolders[i];
          const interfacePath = `${assetsFolderPath}/${interfaceName}`;

          const subInterfacePath = `${interfacePath}/${formatedFileName}.md`;

          if (existsSync(subInterfacePath)) {
            const path = relative(docFilePath, subInterfacePath);
            const linkString = `[${filename}](${path.split(sep).join(posix.sep)})`;
            return linkString;
          }
        }
      }
    }

    // return the fileName if no file found
    return filename;
  }
}
