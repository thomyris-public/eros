import { FormatPath } from './formatPath';

describe('FormatPath', () => {
  const formatPath = new FormatPath();
  it('Should return a formated string', () => {
    // arrange
    const string = 'test@ TestTo/*"';
    // act
    const result = formatPath.execute(string);
    // assert
    expect(result).toStrictEqual('test_testto');
  });
});
