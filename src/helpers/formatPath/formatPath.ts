export interface FormatPathInterface {
  execute(folderName: string): string;
}

export class FormatPath implements FormatPathInterface {
  execute(string: string): string {
    // each words to camelcase
    string = string.toLowerCase();
    // remove special chars
    const formatedString: string = string
      .trim()
      .replace(/\s|:/g, '_')
      .replace(/[\[\]!@#$%^&*(),.?":{}|<>/\\]/g, '');
    return formatedString;
  }
}
