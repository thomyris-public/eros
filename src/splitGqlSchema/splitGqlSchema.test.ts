import { promises } from 'fs';

import { SplitGqlSchema } from './splitGqlSchema';
import { conf, headFile, fileScalar, splitedScalarFile, fileTwoLineComment, splitedTwoLineComment } from './splitGqlSchema.test.data';

describe('SplitGqlSchema', () => {
  const splitSchema = new SplitGqlSchema();
  const spyRead = jest.spyOn(promises, 'readFile');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should call promises.readFile and return nothing', async () => {
    // arrange
    spyRead.mockResolvedValue(headFile);
    // act
    const result: string[] = await splitSchema.execute(conf);
    // assert
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(result).toStrictEqual(['\n\n\n']);
  });

  it('Should return a scalar', async () => {
    // arrange
    spyRead.mockResolvedValue(fileScalar);
    // act
    const result: string[] = await splitSchema.execute(conf);
    // assert
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(result).toStrictEqual(splitedScalarFile);
  });

  it('Should return a string for a two line comment string', async () => {
    // arrange
    spyRead.mockResolvedValue(fileTwoLineComment);
    // act
    const result: string[] = await splitSchema.execute(conf);
    // assert
    expect(spyRead).toHaveBeenCalledTimes(1);
    expect(result).toStrictEqual(splitedTwoLineComment);
  });
});
