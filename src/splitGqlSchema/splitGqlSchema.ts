import { promises } from 'fs';

import { ConfInterface } from '../helpers/getJsonConfig/conf.interface';

export interface splitGqlSchemaInterface {
  execute(conf: ConfInterface): Promise<string[]>;
}
export class SplitGqlSchema implements splitGqlSchemaInterface {
  public async execute(conf: ConfInterface): Promise<string[]> {
    // split the schema
    const fileString: string = await promises.readFile(conf.input, 'utf8');
    const lines: string[] = fileString.split(/(?<=\n)/);

    // loop on each line to change it as we wish it to be
    for (let i = 0; i < lines.length; i++) {
      let element = lines[i];

      //remove gql comments from lines
      const havComment: number = element.indexOf('#');
      if (havComment !== -1) {
        element = element.slice(0, havComment) + '\n';
      }

      // add } to scalars, so we can cut the string later using } and breakline
      if (element.split(' ')[0] === 'scalar') {
        element = element.concat('}\n');
      }

      // remove breakline on comments
      const isTwoLineComment = element.trim() === '"""' && lines[i + 2].trim() === '"""' && !lines[i + 1].includes('{');
      if (isTwoLineComment) {
        element = element.replace(/(\n)|(\r\n)/, '') + lines[i + 1].replace(/(\n)|(\r\n)/, '').trim() + lines[i + 2].trim() + '\r\n';
        lines.splice(i, 1);
        lines.splice(i + 1, 1);
      }
      lines[i] = element;
    }

    // recreate a string with all lines
    const joinedLine: string = lines.join('');

    // split the string by '\n}' to get each objects writen in the string
    const linesByObject: string[] = joinedLine.split(/(?<=\n})/);
    return linesByObject;
  }
}
