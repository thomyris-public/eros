import { ConfInterface } from '../helpers/getJsonConfig/conf.interface';

const conf: ConfInterface = {
  input: '',
  maxDepth: 2,
  output: {
    path: '',
    dirName: '',
  },
  app: {
    title: 'title',
  },
};

const headFile =
  '# ------------------------------------------------------\n# THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)\n# ------------------------------------------------------';
const fileScalar = headFile + '\n"""TextField scalar type"""\nscalar TextField';
const fileTwoLineComment = headFile + '\n"""\ntotal number of received multicast packets. This option is supported only on certain devices.\n"""\nmulticast: Float!';

const splitedScalarFile = ['\n\n\n"""TextField scalar type"""\nscalar TextField}\n'];
const splitedTwoLineComment = ['\n\n\n"""total number of received multicast packets. This option is supported only on certain devices."""\r\nmulticast: Float!'];

export { conf, headFile, fileScalar, splitedScalarFile, fileTwoLineComment, splitedTwoLineComment };
