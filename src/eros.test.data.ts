import { SchemaInterface } from './getSchemaData/schema.interface';
import { ConfInterface } from './helpers/getJsonConfig/conf.interface';

const conf: ConfInterface = {
  app: {
    title: 'title',
  },
  maxDepth: 2,
  input: '',
  output: {
    path: './tests',
    dirName: '',
  },
};

const schema: SchemaInterface = {
  interface: { data: [] },
  type: { data: [] },
  input: { data: [] },
  query: { data: [] },
  mutation: { data: [] },
  subscription: { data: [] },
  scalar: { data: [] },
};

export { conf, schema };
