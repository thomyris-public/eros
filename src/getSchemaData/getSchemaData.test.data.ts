const emptyLines = [''];
const inputLines = ['\r\n\r\ninput GetDhcpByIdInput {\r\n  id: CustomID!\r\n}'];
const scalarLines = ['\r\n\r\n"""Frequency scalar type"""\r\nscalar Frequency\r\n}'];
const scalarContent = '\r\nscalar Frequency\r\n';
const typeLines = ['\r\n\r\ntype EntityServiceDeleteResponseModel implements DeleteResponseType {\r\n  affected: Float!\r\n}'];
const interfaceLines = ['\r\n\r\ninterface AbstractNetworkType {\r\n  type: NETWORK_TYPE!\r\n}'];
const queryLines = ['\r\n\r\ntype Query {\r\ngetVpnClient(input: GetVpnClientResolverDto!): GetVpnClientResolverModel!\r\n}'];
const mutationLines = ['\r\n\r\ntype Mutation {\r\n  """Reset an equipment statistics"""\r\n  resetEquipment(input: EquipmentResetInput!): EquipmentType!\r\n}'];
const subscriptionLines = ['\r\n\r\ntype Subscription {\r\n  """Reset an equipment statistics"""\r\n  resetEquipment(input: EquipmentResetInput!): EquipmentType!\r\n}'];
const enumLines = ['\r\n\r\nenum FIREWALL_ACTION {\r\n  AUTHORIZE\r\n  DROP\r\n  REJECT\r\n}'];

export { emptyLines, inputLines, scalarLines, scalarContent, typeLines, interfaceLines, queryLines, mutationLines, enumLines, subscriptionLines };
