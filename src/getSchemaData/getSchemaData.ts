import { DataElmt, SchemaInterface } from './schema.interface';

export interface GetSChemaDataInterface {
  execute(lines: string[]): Promise<SchemaInterface>;
}

export class GetSchemaData implements GetSChemaDataInterface {
  private schema: SchemaInterface = {
    interface: {
      data: [],
    },
    type: {
      data: [],
    },
    input: {
      data: [],
    },
    enum: {
      data: [],
    },
    query: {
      data: [],
    },
    mutation: {
      data: [],
    },
    subscription: {
      data: [],
    },
    scalar: {
      data: [],
    },
  };

  async execute(lines: string[]): Promise<SchemaInterface> {
    try {
      let baseSchema: SchemaInterface | undefined;

      // remove blank lines
      const filteredLines: string[] = lines.filter((elmt) => !/(^\n$|^\r\n$|^$)/.test(elmt));

      //loop on each line to populate the schema
      for (let i = 0; i < filteredLines.length; i++) {
        const line: string = filteredLines[i];
        const type = line.split(' ')[0].replace(/(\r\n|\n|\r)/gm, '');
        baseSchema = await this.filterLine(type, line, this.schema);
      }

      if (baseSchema === undefined) throw new Error('Did not get data from the schema');
      return baseSchema;
    } catch (error) {
      throw error;
    }
  }

  private async filterLine(type: string, line: string, savedSchema: SchemaInterface | undefined): Promise<any> {
    // check if line is a comment
    if (type.slice(0, 3) === '"""') {
      // get the comment content the realLine and the line type
      const splitedLine = line.split(/(?<=\n)/).filter((elmt) => !(elmt === '\n' || elmt === '\r\n'));
      const type = splitedLine[1].split(' ')[0];
      const comment = splitedLine[0];
      const realLine = '\r\n' + splitedLine.slice(1).join('');
      const result = await this.checkType(type, realLine, comment, savedSchema);
      return result;
    }
    const result = await this.checkType(type, line, '', savedSchema);

    return result;
  }

  private async checkType(type: string, line: string, comment: string, savedSchema: SchemaInterface | undefined) {
    for (const property in savedSchema) {
      let secondType;

      // get if it is a query or a mutation
      if (type === 'type') {
        const nextWord = line
          .split(' ')[1]
          .replace(/(\r\n|\n|\r)/gm, '')
          .toLowerCase();
        if (nextWord === 'query' || nextWord === 'mutation' || nextWord === 'subscription') secondType = nextWord;
      }

      // add data to proper property
      if ((type === property && !secondType) || secondType === property) {
        if (property === 'scalar') line = line.slice(0, -1);
        const data = this.concatLine(line, comment);

        this.schema[property].data.push(data);
        return this.schema;
      }
    }
  }

  private concatLine(line: string, comment: string): DataElmt {
    const result = {
      fileName: line.replace(/(\r\n|\n|\r)/gm, '').split(' ')[1],
      comment: comment.replace(/(\r\n|\n|\r)|"""/gm, ''),
      fileContent: line,
    };
    return result;
  }
}
