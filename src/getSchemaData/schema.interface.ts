export interface SchemaInterface {
  [key: string]: SchemaProperty;
  interface: SchemaProperty;
  type: SchemaProperty;
  input: SchemaProperty;
  query: SchemaProperty;
  subscription: SchemaProperty;
  mutation: SchemaProperty;
  scalar: SchemaProperty;
}

export interface SchemaProperty {
  data: DataElmt[];
}

export interface DataElmt {
  fileName: string;
  comment?: string;
  fileContent: string;
}
