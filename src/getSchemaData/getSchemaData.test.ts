import { GetSchemaData } from './getSchemaData';
import { SchemaInterface } from './schema.interface';
import { enumLines, inputLines, interfaceLines, mutationLines, queryLines, scalarContent, scalarLines, subscriptionLines, typeLines } from './getSchemaData.test.data';

describe('GetSchemaData', () => {
  const getSchema = new GetSchemaData();

  // add "as any" to spy private method
  const spyFilter = jest.spyOn(getSchema as any, 'filterLine');
  const spyCheck = jest.spyOn(getSchema as any, 'checkType');
  const spyConcat = jest.spyOn(getSchema as any, 'concatLine');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  it('Should return an empty schema and call needed private method', async () => {
    // arrange
    // act
    let result: SchemaInterface | any;
    try {
      result = await getSchema.execute(['']);
    } catch (error) {
      result = error;
    }
    // assert
    expect(spyFilter).not.toHaveBeenCalled();
    expect(spyCheck).not.toHaveBeenCalled();
    expect(spyConcat).not.toHaveBeenCalled();
    expect(result).toStrictEqual(new Error('Did not get data from the schema'));
  });

  it('Should return a schema with an input', async () => {
    // arrange
    // act
    let result: SchemaInterface | any;
    try {
      result = await getSchema.execute(inputLines);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result.input.data[0].fileContent).toStrictEqual(inputLines[0]);
  });

  it('Should return a schema with a scalar with a comment', async () => {
    // arrange
    // act
    let result: SchemaInterface | any;
    try {
      result = await getSchema.execute(scalarLines);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result.scalar.data[0].fileContent).toStrictEqual(scalarContent);
  });

  it('Should return a schema with a query', async () => {
    // arrange
    // act
    let result: SchemaInterface | any;
    try {
      result = await getSchema.execute(queryLines);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result.query.data[0].fileContent).toStrictEqual(queryLines[0]);
  });

  it('Should return a schema with a mutation', async () => {
    // arrange
    // act
    let result: SchemaInterface | any;
    try {
      result = await getSchema.execute(mutationLines);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result.mutation.data[0].fileContent).toStrictEqual(mutationLines[0]);
  });

  it('Should return a schema with a subscription', async () => {
    // arrange
    // act
    let result: SchemaInterface | any;
    try {
      result = await getSchema.execute(subscriptionLines);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result.subscription.data[0].fileContent).toStrictEqual(subscriptionLines[0]);
  });

  it('Should return a schema with a type', async () => {
    // arrange
    // act
    let result: SchemaInterface | any;
    try {
      result = await getSchema.execute(typeLines);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result.type.data[0].fileContent).toStrictEqual(typeLines[0]);
  });

  it('Should return a schema with an interface', async () => {
    // arrange
    // act
    let result: SchemaInterface | any;
    try {
      result = await getSchema.execute(interfaceLines);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result.interface.data[0].fileContent).toStrictEqual(interfaceLines[0]);
  });

  it('Should return a schema with an enum', async () => {
    // arrange
    // act
    let result: SchemaInterface | any;
    try {
      result = await getSchema.execute(enumLines);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result.enum.data[0].fileContent).toStrictEqual(enumLines[0]);
  });
});
