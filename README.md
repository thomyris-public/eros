[![npm version](https://img.shields.io/npm/v/eros_gql.svg)](https://www.npmjs.com/package/eros_gql)
# Eros_gql
---

This project generate a doc for a [GraphQL](https://graphql.org/) API created with [Nest.js](https://nestjs.com/).

Do not support schema with directives, subscriptions, comments and union type for the moment.

## Use

- node `^v16.13.2`
- [randexp](https://github.com/fent/randexp.js)
## How to

Install the package.

```bash
npm i eros_gql
```

In your `package.json` script add a script `"eros": "eros_gql"`.

Add a conf file named `eros.conf.json` (example below).

Running the script will generate a file `eros.data.json` that save all generated value, you can edit the file to change the value generated in your documentation.

Run your script.

```bash
npm run eros
```

## Example eros.conf.json:

```json
{
  "input": "./gqlschema.gql",
  "output": {
    "path": "./genDoc/",
    "dirName": "api"
  },
  "maxDepth": 2,
  "app": {
    "title": "app Title",
    "description": "app description"
  },
  "tree": {
    "user": [
      {
        "requestName": "users",
        "errors": [
          {
            "name": "NOT_FOUND",
            "reason": "User not found"
          }
        ]
      },
      {
        "requestName": "createUser"
      }
    ],
    "post": [
      {
        "requestName": "posts"
      },
      {
        "requestName": "createPost"
      }
    ]
  },
  "scalars":[
    {
      "name": "TextField",
      "regex": "^[a-zA-Z0-9-/'.@ çàèéùâêîôûäëïöü]*$"
    },
    {
      "name": "CustomID",
      "regex": "^[a-zA-Z0-9-/'.@ çàèéùâêîôûäëïöü]*$"
    }
  ]
}
```
## Conf documentation

### 1. Script conf

  - `input` is the path to your graphl schema.

  - `output` have two properties.
      - `path` the path where the documentation will be generated.
      - `dirName` the name of the directory that will contain all the genrated files.

  - `maxDepth` used to avoid infinite loop.
### 2. Defining your documentation:

#### app
Your app information.

  - `title` is the app's title.

  - `desciption` is the app's description , `optionable`.

#### tree
Your documentation structure.

Each property is a folder with a list of request's name from your gql schema.

Here `user` will be a folder containing the  request `users`.

Each request need a `requestName` property and can have a list of `errors`.

Request specified in a `folder` will be listed under the `folder` name in the table of content.

#### scalars
Specify your custom `scalars`.

An array of object with properties `name` and `regex`.

Only support regex for now.

The specified scalars will appear in the table of content.